import { Grid } from 'antd';

const { useBreakpoint } = Grid;

const useResponsive = () => {
  const { xl, md, xs } = useBreakpoint();

  const scrXl = xl;
  const scrMd = !xl && md;
  const scrXs = xs;

  return { scrXl, scrMd, scrXs };
};

export default useResponsive;
