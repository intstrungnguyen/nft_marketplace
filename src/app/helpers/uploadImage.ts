import { storage } from 'app/firebase/config';
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { v4 } from 'uuid';

export const uploadImageToFirebase = async (image: any) => {
  if (image.type !== 'image/jpeg' && image.type !== 'image/png') {
    alert('Vui lòng chọn file ảnh');
    return;
  }

  const imageRef = ref(storage, `avatars/${v4() + image.name}`);

  try {
    await uploadBytes(imageRef, image);
    const url = await getDownloadURL(imageRef);
    return {
      status: 'ok',
      url,
    };
  } catch (error) {
    console.log('Upload Image Failed', error);
    return {
      status: 'error',
      error: error,
    };
  }
};
