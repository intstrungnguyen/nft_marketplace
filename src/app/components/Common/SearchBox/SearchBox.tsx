import React, { useState, ChangeEvent } from 'react';
import './SearchBox.scss';
import { MagnifyingGlass } from 'app/components/Icons/svg';

type Props = {
  handleSearch?: (value: string) => void;
  onClickSearch?: (value: string) => void;
  placeholder?: string;
  disabled?: boolean;
};

const SearchBox: React.FC<Props> = ({
  handleSearch,
  placeholder,
  disabled = false,
  onClickSearch,
}) => {
  const [value, setValue] = useState<string>('');

  const onChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.currentTarget.value);
    handleSearch && handleSearch(e.currentTarget.value);
  };

  const handleClickSearch = () => {
    onClickSearch && onClickSearch(value);
  };

  return (
    <div className="search-box">
      <div className="search-box-content">
        <input
          disabled={disabled}
          className="search-box-input"
          placeholder={placeholder}
          value={value}
          onChange={onChangeInput}
        />
        <div className="search-box-icon" onClick={handleClickSearch}>
          <MagnifyingGlass size={24} />
        </div>
      </div>
    </div>
  );
};

export default SearchBox;
