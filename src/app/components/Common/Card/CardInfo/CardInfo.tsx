import React from 'react';
import './CardInfo.scss';
import clsx from 'clsx';
import { Skeleton } from 'antd';

type Props = {
  isLoading?: boolean;
  info: {
    cardName?: string;
    avatar?: string;
    userName?: string;
    description?: string;
  };
};

const CardInfo: React.FC<Props> = ({ isLoading, info }) => {
  const { cardName, avatar, userName, description } = info;

  return (
    <div className={clsx('box-info')}>
      <Skeleton loading={isLoading}>
        <h5>{cardName}</h5>
        {description ? (
          <p>{description}</p>
        ) : (
          <div className="info">
            <div className="info-avatar">
              <img loading="lazy" alt="" src={avatar} width={24} />
            </div>
            {userName}
          </div>
        )}
      </Skeleton>
    </div>
  );
};

export default CardInfo;
