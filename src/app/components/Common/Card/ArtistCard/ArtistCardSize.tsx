import React from 'react';
import './ArtistCard.scss';
import clsx from 'clsx';

type Props = {
  avatar?: string;
  userName?: string;
  size?: 'sm' | 'md' | 'lg';
};

const ArtistCardSize: React.FC<Props> = ({ size = 'sm', avatar, userName }) => {
  return (
    <div className={clsx('artist-card-size', size)}>
      <div
        className="artist-card-box-image"
        style={{
          width: size === 'sm' ? 30 : 60,
          height: size === 'sm' ? 30 : 60,
        }}
      >
        <img
          loading="lazy"
          alt=""
          src={avatar}
          // style={{
          //   width: size === 'sm' ? 30 : 60,
          //   height: size === 'sm' ? 30 : 60,
          // }}
        />
      </div>
      {size === 'sm' ? userName : <h5>{userName}</h5>}
    </div>
  );
};

export default ArtistCardSize;
