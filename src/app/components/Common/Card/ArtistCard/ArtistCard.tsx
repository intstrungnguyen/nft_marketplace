import React from 'react';
import './ArtistCard.scss';
import useResponsive from 'app/responsive/useResponsive';
import clsx from 'clsx';
import NumberBox from '../../UI/NumberBox/NumberBox';

type TAtist = {
  avatar?: string;
  info?: React.ReactNode;
  rank: number;
  onClickCard?: () => void;
};

const ArtistCard: React.FC<TAtist> = ({ avatar, info, rank, onClickCard }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens = '';
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div className={clsx('artist-card secondary-background', screens)} onClick={onClickCard}>
      <div className="artist-card-image">
        <img loading="lazy" alt="" src={avatar} />
      </div>
      {info}

      <NumberBox
        type="rank"
        quantity={rank}
        locate={scrXl ? { top: 18, left: 20 } : { top: 12, left: 13 }}
      />
    </div>
  );
};

export default ArtistCard;
