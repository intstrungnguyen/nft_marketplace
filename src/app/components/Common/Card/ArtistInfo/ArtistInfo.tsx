import React from 'react';
import clsx from 'clsx';
import './ArtistInfo.scss';
import useResponsive from 'app/responsive/useResponsive';

type TInfo = {
  type?: 'horizontal' | 'vertical';
  name?: string;
  title?: string;
  total?: string | number;
};

const ArtistInfo: React.FC<TInfo> = ({ type = 'horizontal', name, title, total }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens = '';
  if (scrXl) screens = 'desktop';
  if (scrMd) screens = 'tablet';
  if (scrXs) screens = 'mobile';

  return (
    <div className={clsx('artist-info', type, screens)}>
      {type === 'horizontal' && <h5>{name}</h5>}
      <span className={clsx('artist-info-content')}>
        <p className="artist-info-title SpaceMono-font caption">{title}</p>
        <p className="SpaceMono-font">{total}</p>
      </span>
    </div>
  );
};

export default ArtistInfo;
