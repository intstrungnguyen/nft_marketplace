import React from 'react';
import { Skeleton } from 'antd';
import './Card.scss';
import clsx from 'clsx';
import { Plus } from 'app/components/Icons/svg';
import ListImageBox from '../UI/ImageBox/ListImageBox';
import ArtistInfo from './ArtistInfo/ArtistInfo';
import useResponsive from 'app/responsive/useResponsive';

type Card = {
  isLoading?: boolean;
  type?: 'normal' | 'collection' | 'discover' | 'work' | 'create';
  cardBg?: string;
  imagePlaceholder?: string;
  imagePlaceholderComp?: React.ReactNode;
  listImage?: {
    size: number;
    listNfts: [];
  };
  cardInfo?: React.ReactNode;
  artistInfo?: {
    title?: string;
    total?: string | number;
  }[];
  onClick?: () => void;
};

const Card: React.FC<Card> = ({
  isLoading = false,
  type = 'normal',
  cardBg,
  imagePlaceholder,
  imagePlaceholderComp,
  listImage,
  cardInfo,
  artistInfo,
  onClick,
}) => {
  const firstTwoImage = listImage?.listNfts?.slice(0, 2);
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div
      className={clsx(
        'card',
        cardBg,
        type,
        screens,
        type === 'collection' ? '' : cardBg ? cardBg : 'secondary-background',
      )}
      onClick={onClick}
    >
      {type === 'create' ? (
        <div>
          <Plus size={60} />
        </div>
      ) : (
        <>
          <div className="card-image">
            {type === 'work' ? (
              imagePlaceholderComp
            ) : (
              <div className="card-image-placeholder">
                <Skeleton
                  active
                  loading={isLoading}
                  avatar={{ shape: 'square', size: 330 }}
                  paragraph={false}
                  title={false}
                >
                  <img loading="lazy" alt="" src={imagePlaceholder} />
                </Skeleton>
              </div>
            )}
            {type === 'collection' && (
              <div className="card-image-secondary">
                {firstTwoImage &&
                  firstTwoImage?.map(
                    (item: any, index) =>
                      item.image !== '' && (
                        <ListImageBox
                          isLoading={isLoading}
                          key={index}
                          size={scrXs ? 95 : listImage?.size}
                          url={item.image}
                        />
                      ),
                  )}
                <ListImageBox
                  isLoading={isLoading}
                  size={scrXs ? 95 : listImage?.size}
                  more={`${listImage?.listNfts?.length}`}
                />
              </div>
            )}
          </div>

          <div className="card-info">
            {cardInfo}
            {type === 'discover' && (
              <div className="card-discover">
                {artistInfo &&
                  artistInfo.map((artist, index) => (
                    <ArtistInfo type="vertical" title={artist.title} total={artist.total} />
                  ))}
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default Card;
