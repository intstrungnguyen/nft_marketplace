import React from 'react';
import clsx from 'clsx';
import './Highlight.scss';
import useResponsive from 'app/responsive/useResponsive';

type Props = {
  type?: 'cover-photo';
  backgroundImage: string;
};

const Highlight: React.FC<Props> = ({ type = '', backgroundImage, children }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div className={clsx('highlight', type)}>
      <img loading="lazy" className={clsx('highlight-img', screens)} src={backgroundImage} />
      <div className="linear-gradient-bg" />
      {children}
    </div>
  );
};

export default Highlight;
