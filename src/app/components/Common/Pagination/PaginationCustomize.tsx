import React from 'react';
import { Pagination } from 'antd';
import './PaginationCustomize.scss';

type Props = {
  total: number;
  onChangePage: (page: number, pageSize: number) => void;
};

const PaginationCustomize: React.FC<Props> = ({ total, onChangePage }) => {
  return (
    <Pagination
      total={total}
      defaultCurrent={1}
      defaultPageSize={9}
      onChange={onChangePage}
      pageSizeOptions={['9', '18']}
    />
  );
};

export default PaginationCustomize;
