import useResponsive from 'app/responsive/useResponsive';
import React from 'react';
import { Responsive } from 'types/common';

type Props = {
  items?: {
    title: string;
    content: string;
  }[];
};

const Desktop: React.FC<{ title: string; content: string }> = ({ title, content }) => {
  return (
    <div style={{ width: 150 }}>
      <h4 className="SpaceMono-font">{title}</h4>
      <p style={{ fontSize: 24 }}>{content}</p>
    </div>
  );
};

const TabletAndMobile: React.FC<{ title: string; content: string }> = ({ title, content }) => {
  return (
    <div style={{ width: 90 }}>
      <h5 className="SpaceMono-font">{title}</h5>
      <p>{content}</p>
    </div>
  );
};

const ResponsiveAdditionalInfo: React.FC<{
  item: { title: string; content: string };
  responsive?: { scrXl?: boolean; scrMd?: boolean; scrXs?: boolean };
}> = ({ item, responsive }) => {
  if (responsive?.scrXl) {
    return <Desktop title={item.title} content={item.content} />;
  } else {
    return <TabletAndMobile title={item.title} content={item.content} />;
  }
};

const AdditionalInfo: React.FC<Props> = ({ items }) => {
  const responsive = useResponsive();

  return (
    <div style={{ display: 'flex', gap: 30 }}>
      {items &&
        items.length > 0 &&
        items.map((item, index) => (
          <ResponsiveAdditionalInfo key={index} item={item} responsive={responsive} />
        ))}
    </div>
  );
};

export default AdditionalInfo;
