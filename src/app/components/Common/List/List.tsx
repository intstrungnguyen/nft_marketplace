import React from 'react';
import './List.scss';
import clsx from 'clsx';
import useResponsive from 'app/responsive/useResponsive';

type Props = {
  type?: 'normal' | 'secondary';
  logo?: React.ReactNode;
  head?: string;
  items?: string[] | React.ReactNode[];
  contacts?: React.ReactNode[];
};

const List: React.FC<Props> = ({ type = 'normal', logo, head, items, contacts }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div className={clsx('list', type)}>
      {logo ? logo : <h5 className="SpaceMono-font">{head}</h5>}
      <ul>
        {items && items.length > 0 && items.map((item, index) => <li key={index}>{item}</li>)}
        {contacts && (
          <>
            <div className="list-contact">
              {contacts && contacts.length > 1 && contacts.map((contact, _) => contact)}
            </div>
            <div className={clsx('contact', screens)}>
              {contacts && contacts.length === 1 && contacts}
            </div>
          </>
        )}
      </ul>
    </div>
  );
};

export default List;
