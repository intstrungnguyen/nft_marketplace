import React, { useState } from 'react';
import { Row, Col } from 'antd';
import clsx from 'clsx';
import './TabBar.scss';
import NumberBox from '../NumberBox/NumberBox';

type Props = {
  setTab: (label: number) => void;
  items: {
    label: string;
    quantity: number;
  }[];
};

const TabBar: React.FC<Props> = ({ setTab, items }) => {
  const [active, setActive] = useState<number>(0);

  const handleTab = (index: number) => {
    setActive(index);
    setTab(index);
  };

  return (
    <Row className="tabbar">
      {items.map((item, index) => (
        <Col key={index} span={24 / items.length}>
          <div className={clsx('tabbar-item', active === index && 'active')}>
            <h5 onClick={() => handleTab(index)}>{item.label}</h5>
            <div>
              <NumberBox
                type="tab"
                highlight={active === index && true}
                quantity={item.quantity}
                absolute={false}
              />
            </div>
          </div>
        </Col>
      ))}
    </Row>
  );
};

export default TabBar;
