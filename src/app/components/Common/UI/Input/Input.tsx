import React, { ChangeEvent } from 'react';
import clsx from 'clsx';
import './Input.scss';

type Props = {
  label?: string;
  type?: 'horizontal' | 'vertical';
  typeInput?: string;
  value: string | number;
  name: string;
  placeholder?: string;
  disabled?: boolean;
  onChangeInput: (e: ChangeEvent<HTMLInputElement>, key: string) => void;
  onChangeImage?: (e: ChangeEvent<HTMLInputElement>) => void;
};

const Input: React.FC<Props> = ({
  label,
  type = 'horizontal',
  typeInput = 'text',
  value,
  name,
  placeholder,
  disabled = false,
  onChangeInput,
  onChangeImage,
}) => {
  return (
    <div className={clsx('box-input', type)}>
      {label && (
        <label htmlFor="">
          <h5>{label}</h5>
        </label>
      )}

      {typeInput !== 'file' ? (
        <input
          disabled={disabled}
          className={clsx('input-normal', disabled ? 'disabled' : '')}
          type={typeInput}
          name={name}
          placeholder={placeholder}
          value={value ? value : ''}
          onChange={(e) => onChangeInput(e, name)}
        />
      ) : (
        <label className="upload-image" htmlFor="image">
          <input
            disabled={disabled}
            id="image"
            className="input-image"
            type={typeInput}
            name={name}
            placeholder={placeholder}
            value={value ? value : ''}
            accept="image/*"
            onChange={(e) => onChangeImage && onChangeImage(e)}
          />
          <p>Upload Image</p>
        </label>
      )}
    </div>
  );
};

export default Input;
