import React from 'react';
import './CopyRight.scss';
import clsx from 'clsx';
import useResponsive from 'app/responsive/useResponsive';

type Props = {};

const CopyRight = (props: Props) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div className={clsx('copyright', screens)}>
      <p className="copyright-text">Ⓒ NFT Market. Use this template freely.</p>
    </div>
  );
};

export default CopyRight;
