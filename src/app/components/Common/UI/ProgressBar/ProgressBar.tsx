import React, { useState } from 'react';
import { Progress } from 'antd';
import clsx from 'clsx';
import './ProgressBar.scss';
import { useSelector } from 'react-redux';

type Props = {};

const ProgressBar: React.FC<Props> = (props: Props) => {
  const progressState = useSelector((state: any) => state.progress);
  const { percent, loading } = progressState;

  return (
    <Progress
      className={clsx(loading && 'hiden')}
      percent={percent}
      status="active"
      showInfo={false}
      strokeWidth={4}
      strokeColor={{
        '0%': '#bf8eff',
        '100%': '#a259ff',
      }}
    />
  );
};

export default ProgressBar;
