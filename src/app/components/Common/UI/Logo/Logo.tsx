import React from 'react';
import { useHistory } from 'react-router-dom';
import { Storefront } from 'app/components/Icons/svg';
// import NFTMartketplace from '../../../../images/NFT Marketplace.png';
import NFTMartketplace from 'app/components/Icons/Logo/NFTMartketplace';

type Props = {
  sizeLogo: number | any;
};

const Logo: React.FC<Props> = ({ sizeLogo }) => {
  const history = useHistory();

  const onClick = () => {
    history.push('/');
  };

  return (
    <div
      className="flex items-end"
      style={{ height: sizeLogo, gap: 10, cursor: 'pointer' }}
      onClick={onClick}
    >
      <Storefront size={sizeLogo} color="#a259ff" />
      <NFTMartketplace />
    </div>
  );
};

export default Logo;
