// Button.tsx
import React from 'react';
import c from 'clsx';

export type TButtonProps = {
  className?: string;
  justify?: string;
  variant: 'primary' | 'secondary' | 'tertiary' | string;
  type?: 'outline' | 'link';
  icon?: React.ReactNode;
  htmlType?: 'button' | 'submit' | 'reset';
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

const Button: React.FC<TButtonProps> = ({
  className,
  justify = 'justify-center',
  variant,
  type,
  icon,
  onClick,
  htmlType,
  children,
}) => {
  return (
    <button
      onClick={onClick}
      className={c(`flex items-center`, justify, variant, type, className)}
      type={htmlType}
    >
      {icon}
      {variant === 'primary' ? (
        <h5>{children}</h5>
      ) : (
        <p className={c(className, 'label-button')}>{children}</p>
      )}
    </button>
  );
};

export default Button;
