import React, { ChangeEvent } from 'react';
import './ImageBox.scss';
import clsx from 'clsx';
import { EditOutlined } from '@ant-design/icons';

type Props = {
  size: {
    width: number;
    height: number;
  };
  url?: string;
  border?: boolean;
  edit?: boolean;
  onChangeImage?: (e: ChangeEvent<HTMLInputElement>) => void;
};

const ImageBox: React.FC<Props> = ({ size, url, border = false, edit = false, onChangeImage }) => {
  return (
    <div
      className={clsx('box', border ? 'border-box' : '')}
      style={{ width: size.width, height: size.height }}
    >
      <img src={url} />
      {edit && (
        <div className="box-edit-image" style={{ width: size.width, height: size.height }}>
          <label className="upload-image" htmlFor="edit-image">
            <input
              id="edit-image"
              className="box-edit-input"
              type="file"
              accept="image/*"
              onChange={(e) => onChangeImage && onChangeImage(e)}
            />
            <EditOutlined className="box-edit-icon" />
          </label>
        </div>
      )}
    </div>
  );
};

export default ImageBox;
