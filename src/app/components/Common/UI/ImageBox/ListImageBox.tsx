import React from 'react';
import './ImageBox.scss';
import clsx from 'clsx';
import { Skeleton } from 'antd';

type Props = {
  isLoading?: boolean;
  size?: number;
  url?: string;
  more?: string;
};

const ListImageBox: React.FC<Props> = ({ isLoading, size, url, more }) => {
  return (
    <div
      className={clsx('box', 'box-transition', more && 'more')}
      style={{ width: size, height: size }}
    >
      <Skeleton
        active
        loading={isLoading}
        avatar={{ shape: 'square', size: 120 }}
        paragraph={false}
        title={false}
      >
        {url ? <img src={url} /> : null}
        {more ? <h5 className="SpaceMono-font">{`${more}+`}</h5> : null}
      </Skeleton>
    </div>
  );
};

export default ListImageBox;
