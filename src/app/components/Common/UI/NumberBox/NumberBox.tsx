import React from 'react';
import clsx from 'clsx';
import './NumberBox.scss';

type Props = {
  type: 'rank' | 'tab';
  quantity: number;
  absolute?: boolean;
  locate?: {
    top?: number;
    right?: number;
    bottom?: number;
    left?: number;
  };
  highlight?: boolean;
};

const NumberBox: React.FC<Props> = ({
  type,
  quantity,
  absolute = true,
  locate,
  highlight = false,
}) => {
  return (
    <div
      className={clsx('box-number', type, absolute && 'absolute', highlight && 'highlight-tab')}
      style={{ top: locate?.top, right: locate?.right, bottom: locate?.bottom, left: locate?.left }}
    >
      <p className="SpaceMono-font caption">{quantity}</p>
    </div>
  );
};

export default NumberBox;
