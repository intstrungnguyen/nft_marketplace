import React from 'react';
import './ButtonGroup.scss';
import Button from '../Button/Button';
import clsx from 'clsx';
import { TButtonProps } from '../Button/Button';

type TButtonGroup = TButtonProps & {
  label: string;
};

type Props = {
  items: TButtonGroup[];
  justify?: 'start' | 'end' | 'center';
};

const ButtonGroup: React.FC<Props> = ({ items, justify }) => {
  return (
    <div className={clsx('button-group', justify)}>
      {items.map((item, index) => (
        <Button
          key={index}
          variant={item.variant}
          type={item.type}
          icon={item.icon}
          onClick={item.onClick}
        >
          {item.label}
        </Button>
      ))}
    </div>
  );
};

export default ButtonGroup;
