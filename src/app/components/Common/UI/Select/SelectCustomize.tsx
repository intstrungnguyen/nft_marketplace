import React from 'react';
import type { SelectProps } from 'antd';
import { Select } from 'antd';
import './SelectCustomize.scss';

const options: SelectProps['options'] = [];

for (let i = 10; i < 36; i++) {
  options.push({
    value: i.toString(36) + i,
    label: i.toString(36) + i,
  });
}

type Props = {};

const SelectCustomize: React.FC = (props: Props) => {
  const handleChange = (value: string) => {
    // console.log(`selected ${value}`);
  };

  return (
    <Select
      mode="tags"
      style={{ width: '100%' }}
      onChange={handleChange}
      tokenSeparators={[',']}
      options={options}
      dropdownStyle={{ backgroundColor: '#3b3b3b' }}
      listHeight={180}
    />
  );
};

export default SelectCustomize;
