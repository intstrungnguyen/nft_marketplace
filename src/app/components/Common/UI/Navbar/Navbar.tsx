import React, { useState } from 'react';
import './navbar.scss';
import clsx from 'clsx';
import ProgressBar from '../ProgressBar/ProgressBar';

type Props = {
  size: string;
};

const Navbar: React.FC<Props> = ({ size, children }) => {
  const [hiden, setHiden] = useState<boolean>(false);
  const [percent, setPercent] = useState<number>(0);

  return (
    <>
      <nav className={clsx('header-navbar', `${size}`)}>{children}</nav>
      <ProgressBar />
    </>
  );
};

export default Navbar;
