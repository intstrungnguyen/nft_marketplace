import React, { useState, useEffect } from 'react';
import './AuctionTimer.scss';
import Button from '../Button/Button';
import clsx from 'clsx';

type Props = {
  secondsRemaining?: number;
  button?: boolean;
  labelButton?: string;
  onClick?: (labelButton: string) => void;
};

const AuctionTimer: React.FC<Props> = ({
  secondsRemaining,
  button = false,
  labelButton,
  onClick,
}) => {
  const countdown = [
    {
      title: 'Hours',
      time: 59,
    },
    {
      title: 'Minutes',
      time: 59,
    },
    {
      title: 'Seconds',
      time: 59,
    },
  ];

  return (
    <div className="auction-timer">
      <p className="SpaceMono-font auction-timer-title">Auction ends in:</p>
      <div className={clsx('auction-timer-countdown', button && 'and-button')}>
        {countdown.map((item, index) => (
          <React.Fragment key={index}>
            <div style={{ display: 'grid', gap: 5 }}>
              <h3 className="SpaceMono-font ">{item.time}</h3>
              <p className="SpaceMono-font auction-timer-time">{item.title}</p>
            </div>
            {index !== countdown.length - 1 && <h4>:</h4>}
          </React.Fragment>
        ))}
      </div>
      {button && (
        <Button
          className="width-full"
          variant="secondary"
          onClick={() => onClick && onClick(labelButton ? labelButton : '')}
        >
          {labelButton}
        </Button>
      )}
    </div>
  );
};

export default AuctionTimer;
