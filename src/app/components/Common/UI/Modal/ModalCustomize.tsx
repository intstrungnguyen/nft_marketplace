import React, { Dispatch, SetStateAction, useState } from 'react';
import { Modal } from 'antd';
import './ModalCustomize.scss';
import ButtonGroup from '../ButtonGroup/ButtonGroup';
import Input from '../Input/Input';

type Props = {
  labelModal?: string;
  isOpen: boolean;
  onCancel: () => void;
  onSubmit: (labelModal: string) => void;
};

const ModalCustomize: React.FC<Props> = ({ labelModal, isOpen, onCancel, onSubmit, children }) => {
  return (
    <>
      <Modal
        className="modal-customize"
        open={isOpen}
        title={labelModal}
        onCancel={onCancel}
        footer={
          <ButtonGroup
            justify="end"
            items={[
              {
                type: 'outline',
                label: 'Cancel',
                variant: 'tertiary',
                onClick: onCancel,
              },
              {
                label: 'Submit',
                variant: 'tertiary',
                onClick: () => onSubmit(labelModal ? labelModal : ''),
              },
            ]}
          />
        }
      >
        {children}
      </Modal>
    </>
  );
};

export default ModalCustomize;
