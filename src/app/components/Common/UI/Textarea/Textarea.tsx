import React, { ChangeEvent } from 'react';
import clsx from 'clsx';
import './Textarea.scss';

type Props = {
  disabled?: boolean;
  name: string;
  value?: string;
  onChangeInput: (e: ChangeEvent<HTMLTextAreaElement>, key: string) => void;
};

const Textarea: React.FC<Props> = ({ disabled = false, name, value, onChangeInput }) => {
  return (
    <textarea
      className={clsx('textarea-normal', disabled ? 'disabled' : '')}
      disabled={disabled}
      value={value}
      rows={disabled ? 1 : 5}
      cols={50}
      onChange={(e) => onChangeInput(e, name)}
    />
  );
};

export default Textarea;
