import React from 'react';
import useResponsive from 'app/responsive/useResponsive';

type Props = {
  type?: 'section' | 'card' | 'form';
  headline?: string;
  subHead?: string;
};

const Headline: React.FC<Props> = ({ type = 'section', headline, subHead }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let headlineContent;

  if (scrXl) {
    switch (type) {
      case 'form':
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <h3>{headline}</h3>
            <p>{subHead}</p>
          </div>
        );
        break;

      case 'card':
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <h5>{headline}</h5>
            <p>{subHead}</p>
          </div>
        );
        break;

      default:
        headlineContent = (
          <div style={{ display: 'grid', gap: 20 }}>
            <h1 style={{ width: '90%' }}>{headline}</h1>
            <p style={{ fontSize: 22 }}>{subHead}</p>
          </div>
        );
        break;
    }
  }

  if (scrMd) {
    switch (type) {
      case 'form':
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <h4>{headline}</h4>
            <p>{subHead}</p>
          </div>
        );
        break;

      case 'card':
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <p>{headline}</p>
            <p style={{ fontSize: 12 }}>{subHead}</p>
          </div>
        );
        break;

      default:
        headlineContent = (
          <div style={{ display: 'grid', gap: 20 }}>
            <h3 style={{ width: '90%' }}>{headline}</h3>
            <p>{subHead}</p>
          </div>
        );
        break;
    }
  }

  if (scrXs) {
    switch (type) {
      case 'form':
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <h4>{headline}</h4>
            <p>{subHead}</p>
          </div>
        );
        break;

      case 'card':
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <p style={{ fontWeight: 600 }}>{headline}</p>
            <p style={{ fontSize: 12 }}>{subHead}</p>
          </div>
        );
        break;

      default:
        headlineContent = (
          <div style={{ display: 'grid', gap: 10 }}>
            <h4 style={{ width: '100%' }}>{headline}</h4>
            <p>{subHead}</p>
          </div>
        );
        break;
    }
  }

  return <>{headlineContent}</>;
};

export default Headline;
