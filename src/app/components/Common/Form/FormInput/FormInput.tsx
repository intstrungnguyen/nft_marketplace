import React, { useState } from 'react';
import './FormInput.scss';
import clsx from 'clsx';
import Button from '../../UI/Button/Button';
import useResponsive from 'app/responsive/useResponsive';

type Props = {
  isLoading?: boolean;
  labelBtn: string;
  items: {
    icon?: React.ReactNode;
    placeholder: string;
    type: string;
    name: string;
  }[];
  handleSubmit: (data: any) => void;
};

const FormInput: React.FC<Props> = ({ isLoading = false, labelBtn, items, handleSubmit }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  const [data, setData] = useState<{ [key: string]: string | number }>({});

  const handleOnSubmit = (e: any) => {
    e.preventDefault();
    handleSubmit(data);
    setData({});
  };

  const handleOnChange = (e: any, field: string) => {
    e.preventDefault();
    setData({
      ...data,
      [field]: e.target.value,
    });
  };

  return (
    <form className={clsx('form', scrXl ? '' : 'width-full')} onSubmit={handleOnSubmit}>
      <div className="form-input">
        {items &&
          items.map((item, index) => (
            <div key={index}>
              <div className="form-icon">{item.icon}</div>
              <input
                placeholder={item.placeholder}
                type={item.type}
                onChange={(e) => handleOnChange(e, item.name)}
                value={data[item.name]}
              />
            </div>
          ))}
      </div>

      <div className="form-btn">
        <Button className="width-full" variant="tertiary" htmlType="submit">
          {labelBtn}
        </Button>
      </div>
    </form>
  );
};

export default FormInput;
