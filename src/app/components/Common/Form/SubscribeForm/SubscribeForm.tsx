import React from 'react';
import './SubscribeForm.scss';
import clsx from 'clsx';

type Props = {
  type?: 'nested' | 'vertical';
  size?: 'xl' | 'md' | 'xs';
  input: {
    placeholder: string;
  };
  button: React.ReactNode;
};

const SubscribeForm: React.FC<Props> = ({ type = 'nested', size = 'xl', button, input }) => {
  return (
    <div className={clsx('subscribe-form', type, size)}>
      <input className="subscribe-form-input" placeholder={input.placeholder} />
      <div className="subscribe-form-btn">{button}</div>
    </div>
  );
};

export default SubscribeForm;
