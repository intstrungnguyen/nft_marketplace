import React from 'react';
import useResponsive from 'app/responsive/useResponsive';

type Props = {
  paddingDefault?: boolean;
};

const Container: React.FC<Props> = ({ paddingDefault = true, children }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let widthContainer = '';
  let paddingContainer = '';
  if (scrXl) {
    widthContainer = '1050px';
    paddingContainer = '80px 0px';
  }
  if (scrMd) {
    widthContainer = '690px';
    paddingContainer = '40px 0px';
  }
  if (scrXs) {
    widthContainer = '315px';
    paddingContainer = '40px 0px';
  }

  return (
    <div style={{ padding: paddingDefault ? paddingContainer : 0, width: widthContainer }}>
      {children}
    </div>
  );
};

export default Container;
