import React, { Dispatch, SetStateAction, ChangeEvent } from 'react';
import { useState } from 'react';
import Input from '../../UI/Input/Input';

type Props = {
  value: any;
  setValue: Dispatch<SetStateAction<any>>;
  setImage: Dispatch<SetStateAction<any>>;

  items: {
    label: string;
    name: string;
    typeInput?: string;
    placeholder?: string;
  }[];
};
const FormModal: React.FC<Props> = ({ items, value, setValue, setImage }) => {
  const onChangeInput = (e: ChangeEvent<HTMLInputElement>, key: string) => {
    const valueInput = e.currentTarget.value;

    setValue((prev: any) => {
      return {
        ...prev,
        [key]: valueInput,
      };
    });
  };

  const onChangeImage = (e: any) => {
    const nameImage = e.target.files[0];
    setImage(nameImage);
  };

  return (
    <form style={{ display: 'grid', gap: 15 }}>
      {items.map((item, index) => (
        <Input
          key={index}
          type="vertical"
          label={item.label}
          name={item.name}
          value={value[item.name]}
          typeInput={item.typeInput}
          placeholder={item.placeholder}
          onChangeInput={onChangeInput}
          onChangeImage={onChangeImage}
        />
      ))}
    </form>
  );
};

export default FormModal;
