import React, { ReactNode } from 'react';
import clsx from 'clsx';
import './SectionHeadline.scss';
import useResponsive from 'app/responsive/useResponsive';

type Props = {
  type?: 'normal' | 'headline-button';
  heading: string | ReactNode;
  subHead?: string;
  button?: ReactNode;
};

const Desktop: React.FC<Props> = ({ heading, subHead }) => {
  return (
    <div className={clsx('headline desktop')}>
      <h3>{heading}</h3>
      <p>{subHead}</p>
    </div>
  );
};

const TabletAndMobile: React.FC<Props> = ({ heading, subHead }) => {
  return (
    <div className={clsx('headline tablet-and-mobile')}>
      <h4>{heading}</h4>
      <p>{subHead}</p>
    </div>
  );
};

const SectionHeadline: React.FC<Props> = ({ type = 'normal', heading, subHead, button }) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  return (
    <div className={clsx('section-headline', type)} style={{ marginBottom: scrXl ? 60 : 40 }}>
      {scrXl ? (
        <Desktop heading={heading} subHead={subHead} />
      ) : (
        <TabletAndMobile heading={heading} subHead={subHead} />
      )}
      {type === 'headline-button' && !scrXs && button}
    </div>
  );
};

export default SectionHeadline;
