import React from 'react';
import './Table.scss';
import clsx from 'clsx';
import ArtistCardSize from '../Card/ArtistCard/ArtistCardSize';
import NumberBox from '../UI/NumberBox/NumberBox';

type Props = {
  variant: 'header' | 'item';
  artist?: {
    avatar: string;
    name: string;
  };
  tats: string[] | number[];
  rank?: number;
};

const Table: React.FC<Props> = ({ variant, artist, tats, rank = 0 }) => {
  return (
    <div className={clsx('table header', variant)}>
      <div className="table-content">
        <div className="table-content-title">
          {variant === 'header' ? (
            <>
              <p className="SpaceMono-font caption rank">#</p>
              <p className="SpaceMono-font caption">Artist</p>
            </>
          ) : (
            <>
              <NumberBox absolute={false} type="rank" quantity={rank} />
              <ArtistCardSize avatar={artist?.avatar} userName={artist?.name} size="md" />
            </>
          )}
        </div>
        <div className="table-content-tats">
          {tats.map((tat, index) => (
            <p
              key={index}
              className={clsx(
                'SpaceMono-font tat',
                variant === 'header' && 'caption',
                variant === 'item' && index === 0 && 'highlight-color',
              )}
            >
              {tat}
            </p>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Table;
