import React from 'react';
import { Row, Col } from 'antd';
import clsx from 'clsx';
import './Footer.scss';
import List from 'app/components/Common/List/List';
import Logo from 'app/components/Common/UI/Logo/Logo';
import DiscordLogo from 'app/components/Icons/Logo/DiscordLogo';
import YoutubeLogo from 'app/components/Icons/Logo/YoutubeLogo';
import TwitterLogo from 'app/components/Icons/Logo/TwitterLogo';
import InstagramLogo from 'app/components/Icons/Logo/InstagramLogo';
import SubscribeForm from 'app/components/Common/Form/SubscribeForm/SubscribeForm';
import Button from 'app/components/Common/UI/Button/Button';
import CopyRight from 'app/components/Common/UI/CopyRight/CopyRight';
import useResponsive from 'app/responsive/useResponsive';

type Props = {};

const Footer = (props: Props) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div className="footer">
      <div className={clsx('footer-info', screens)}>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col xl={{ span: 8 }} md={{ span: 24 }} xs={{ span: 24 }}>
            <List
              logo={<Logo sizeLogo={32} />}
              items={['NFT marketplace UI created with Anima for Figma.', 'Join our community']}
              contacts={[<DiscordLogo />, <YoutubeLogo />, <TwitterLogo />, <InstagramLogo />]}
            />
          </Col>
          <Col xl={{ span: 6 }} md={{ span: 24 }} xs={{ span: 24 }}>
            <List head="Explore" items={['Marketplace', 'Rankings', 'Connect a wallet']} />
          </Col>
          <Col xl={{ span: 10 }} md={{ span: 24 }} xs={{ span: 24 }}>
            <List
              head="Join Our Weekly Digest"
              items={['Get exclusive promotions & updates straight to your inbox.']}
              contacts={[
                <SubscribeForm
                  type={scrXs ? 'vertical' : 'nested'}
                  size={scrXs ? 'md' : 'xl'}
                  input={{
                    placeholder: 'Enter your email here',
                  }}
                  button={
                    <Button
                      className={scrXs ? 'width-full' : ''}
                      variant={scrXs ? 'tertiary' : 'secondary'}
                    >
                      Subscribe
                    </Button>
                  }
                />,
              ]}
            />
          </Col>
        </Row>
      </div>
      <CopyRight />
    </div>
  );
};

export default Footer;
