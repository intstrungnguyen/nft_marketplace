import React from 'react';
import './Header.scss';
import type { MenuProps } from 'antd';
import { Dropdown, message } from 'antd';
import { UserOutlined, SettingOutlined, GiftOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';
import { useDispatch } from 'react-redux';
import { logOut } from 'utils/@reduxjs/slice/authSlice';
import { getUserByIdAction } from 'utils/@reduxjs/slice/userSlice';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';

const items: MenuProps['items'] = [
  {
    key: 'profile',
    label: 'Profile',
    icon: <UserOutlined />,
  },
  {
    key: 'giveaways',
    label: 'Giveaways',
    icon: <GiftOutlined />,
  },
  {
    key: 'setting',
    label: 'Setting',
    icon: <SettingOutlined />,
  },
  {
    label: <p>Logout</p>,
    key: 'logout',
  },
];

const DropdownHeader: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const currentUser = getUserFromLocalStorage();

  const handleButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    dispatch(getUserByIdAction(currentUser.id));
    setTimeout(() => {
      history.push(Epath.artistPage);
    }, 1000);
  };

  const handleMenuClick: MenuProps['onClick'] = (e) => {
    switch (e.key) {
      case 'profile':
        dispatch(getUserByIdAction(currentUser.id));
        setTimeout(() => {
          history.push(Epath.artistPage);
        }, 1000);

        break;

      case 'giveaways':
        // console.log('giveaways');

        break;

      case 'setting':
        // console.log('setting');

        break;

      case 'logout':
        dispatch(logOut());
        history.push(Epath.homePage);
        break;

      default:
        break;
    }
  };

  return (
    <Dropdown.Button
      menu={{
        items,
        onClick: handleMenuClick,
      }}
      placement="bottomRight"
      onClick={handleButtonClick}
    >
      <p>Profile</p>
    </Dropdown.Button>
  );
};

export default DropdownHeader;
