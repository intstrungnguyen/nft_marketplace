import React, { useState } from 'react';
import { RootState } from 'types/RootState';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { NavLink, useHistory } from 'react-router-dom';
import { DEFAULT_LANGUAGE } from 'locales/i18n';
import { Epath } from 'app/routes/routesConfig';
import { User, List } from 'app/components/Icons/svg';
import { TScreens } from '../Layout';
import { logOut } from 'utils/@reduxjs/slice/authSlice';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import Navbar from 'app/components/Common/UI/Navbar/Navbar';
import Logo from 'app/components/Common/UI/Logo/Logo';
import Button from 'app/components/Common/UI/Button/Button';
import DropdownHeader from './DropdownHeader';

const navigates = [
  {
    to: Epath.marketplacePage,
    label: 'Marketplace',
  },
  {
    to: Epath.rankingsPage,
    label: 'Rankings',
  },
  {
    to: Epath.connectWallet,
    label: 'Connect a wallet',
  },
];

const Navigation: React.FC = () => {
  const currentUser = getUserFromLocalStorage();
  const history = useHistory();
  const stateAuth = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch();

  const handleOnClickBtn = () => {
    if (stateAuth.authenticated === true) dispatch(logOut());
    if (stateAuth.authenticated === false) history.push('/login');
  };

  return (
    <div className="flex items-center justify-between" style={{ height: 60, width: 607 }}>
      {navigates.map((navigate, index) => (
        <NavLink key={index} to={navigate.to}>
          <Button variant="tertiary" type="link">
            {navigate.label}
          </Button>
        </NavLink>
      ))}
      {!currentUser ? (
        <Button variant="auth" icon={<User size={20} />} onClick={handleOnClickBtn}>
          Login
        </Button>
      ) : (
        <DropdownHeader />
      )}
    </div>
  );
};

const Header: React.FC<TScreens> = ({ screens }) => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();

  const onLogOut = () => {
    i18n.changeLanguage(DEFAULT_LANGUAGE);
    history.push(Epath.loginPage);
  };

  let navigation = null;
  let size = null;
  let sizeLogo;

  if (screens.xl && screens.md) {
    navigation = <Navigation />;
    size = 'desktop';
    sizeLogo = 32;
  }
  if (!screens.xl && screens.md) {
    navigation = <List size={24} />;
    size = 'tablet';
    sizeLogo = 24;
  }
  if (screens.xs) {
    navigation = <List size={24} />;
    size = 'mobile';
    sizeLogo = 24;
  }

  return (
    <Navbar size={`${size}`}>
      <Logo sizeLogo={sizeLogo} />
      {navigation}
    </Navbar>
  );
};

export default Header;
