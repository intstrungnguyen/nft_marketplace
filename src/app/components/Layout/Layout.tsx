import React, { ReactNode } from 'react';
import { Grid } from 'antd';
import Header from './Header/Header';
import Footer from './Footer/Footer';

export type PropsLayout = {
  children: ReactNode;
};

export type TScreens = {
  screens: {
    xl: any;
    md: any;
    xs: any;
  };
};

function Layout({ children }: PropsLayout) {
  const { useBreakpoint } = Grid;
  const { xl, md, xs } = useBreakpoint();

  return (
    <div className="layout">
      <Header screens={{ xl: xl, md: md, xs: xs }} />
      <div className="wrapper">{children}</div>
      <Footer />
    </div>
  );
}

export default Layout;
