import ArrowLeft from './Svg/ArrowLeft';
import ArrowRight from './Svg/ArrowRight';
import Copy from './Svg/Copy';
import EnvelopeSimple from './Svg/EnvelopeSimple';
import Eye from './Svg/Eye';
import EyeSlash from './Svg/EyeSlash';
import Globe from './Svg/Globe';
import List from './Svg/List';
import LockKey from './Svg/LockKey';
import MagnifyingGlass from './Svg/MagnifyingGlass';
import Plus from './Svg/Plus';
import Rocket from './Svg/Rocket';
import RocketLaunch from './Svg/RocketLaunch';
import Storefront from './Svg/Storefront';
import TrendUp from './Svg/TrendUp';
import User from './Svg/User';
import UserCircle from './Svg/UserCircle';
import Wallet from './Svg/Wallet';

import GroupIconWallet from './Group/GroupIconWallet';
import GroupIconCollection from './Group/GroupIconCollection';
import GroupIconCart from './Group/GroupIconCart';

export {
  ArrowLeft,
  ArrowRight,
  Copy,
  EnvelopeSimple,
  Eye,
  EyeSlash,
  Globe,
  List,
  LockKey,
  MagnifyingGlass,
  Plus,
  Rocket,
  RocketLaunch,
  Storefront,
  TrendUp,
  User,
  UserCircle,
  Wallet,
  // Group
  GroupIconWallet,
  GroupIconCollection,
  GroupIconCart,
};
