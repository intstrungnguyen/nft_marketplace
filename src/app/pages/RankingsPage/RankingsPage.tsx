import React, { useEffect, useState } from 'react';
import { TUserRanking } from 'types/common';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'types/RootState';
import { userRankingAction } from 'utils/@reduxjs/slice/userSlice';
import Container from 'app/components/Common/Form/Container';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import RankingsList from './Section/RankingsList';

type Props = {};

const RankingsPage = (props: Props) => {
  // Redux
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state);
  const userRankingState = state.user.dataUserRanking;

  // Hooks
  const [dataUserRanking, setDataUserRanking] = useState<TUserRanking[]>(userRankingState);

  useEffect(() => {
    window.scrollTo(0, 0);
    dispatch(userRankingAction());
  }, []);

  useEffect(() => {
    setDataUserRanking(userRankingState);
  }, [userRankingState]);

  return (
    <>
      <Container>
        <SectionHeadline
          heading="Top Creators"
          subHead="Check out top ranking NFT artists on the NFT Marketplace."
        />
        <RankingsList data={dataUserRanking} />
      </Container>
    </>
  );
};

export default RankingsPage;
