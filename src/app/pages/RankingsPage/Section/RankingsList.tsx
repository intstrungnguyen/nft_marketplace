import React from 'react';
import { Row, Col } from 'antd';
import { TUserRanking } from 'types/common';
import Table from 'app/components/Common/Table/Table';

type Props = {
  data: TUserRanking[];
};

const RankingsList: React.FC<Props> = ({ data }) => {
  return (
    <Row gutter={[20, 20]}>
      <Col span={24}>
        <Table variant="header" tats={[`Change`, `NFTs Sold`, `Volume`]} />
      </Col>
      {data &&
        data.length > 0 &&
        data.map((item, index) => (
          <Col key={index} span={24}>
            <Table
              variant="item"
              artist={{ avatar: item.avatar, name: item.name }}
              tats={[`+1.41%`, `${item.countNfts}`, `${Math.round(item.totalPrice)} ETH`]}
              rank={index + 1}
            />
          </Col>
        ))}
    </Row>
  );
};

export default RankingsList;
