import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'types/RootState';
import { collectionAction, collectionQueryAction } from 'utils/@reduxjs/slice/collectionSlice';
import { getNftByIdAction, getNftByName, nftSearchAction } from 'utils/@reduxjs/slice/nftSlice';
import { formatDate } from 'app/helpers/functions';
import { nftQueryAction } from 'utils/@reduxjs/slice/nftSlice';
import ImageCollectionDefi from '../../images/collection/defi.jpg';
import ImageCollectionGameFi from '../../images/collection/game.jpg';
import ImageCollectionMeme from '../../images/collection/meme.jpg';
import ImageCollectionAi from '../../images/collection/ai.jpg';
import ImageCollectionEducation from '../../images/collection/education.jpg';
import Container from 'app/components/Common/Form/Container';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import TabBar from 'app/components/Common/UI/TabBar/TabBar';
import Card from 'app/components/Common/Card/Card';
import CardInfo from 'app/components/Common/Card/CardInfo/CardInfo';
import SearchBox from 'app/components/Common/SearchBox/SearchBox';
import PaginationCustomize from 'app/components/Common/Pagination/PaginationCustomize';

type Props = {};

const imagePlaceholder = [
  ImageCollectionDefi,
  ImageCollectionGameFi,
  ImageCollectionMeme,
  ImageCollectionAi,
  ImageCollectionEducation,
];

const Marketplace = (props: Props) => {
  const history = useHistory();

  // Redux
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state);

  const nftsState = state.nfts.dataNFTs;

  const searchNftState = state.nfts.dataSearchNFTs;

  const collectionState = state.collection.dataCollection;
  const searchCollectionState = state.collection.dataSearchCollection;

  // Hooks
  const [tab, setTab] = useState<number>(0);
  const [content, setContent] = useState<any[]>(nftsState.results);
  const [totalResults, setTotalResults] = useState<number>(nftsState.totalResults);

  useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(nftQueryAction({ limit: 9, page: 1 }));
    dispatch(collectionAction());
    return () => {};
  }, []);

  useEffect(() => {
    if (tab === 0) {
      setContent(nftsState.results);
      setTotalResults(nftsState.totalResults);
    }
    if (tab === 1)
      setContent(
        collectionState &&
          collectionState.data.map((item: any, index: number) => {
            return {
              ...item,
              image: imagePlaceholder[index],
            };
          }),
      );

    return () => {};
  }, [tab, nftsState, collectionState]);

  useEffect(() => {
    if (tab === 0) {
      if (searchNftState.results?.length > 0) {
        setContent(searchNftState.results);
        setTotalResults(searchNftState.totalResults);
      }
      if (searchNftState[0] === 'not result') {
        setContent([]);
        setTotalResults(0);
      }

      if (searchNftState[0] === 'not value') {
        setContent(nftsState.results);
        setTotalResults(nftsState.totalResults);
      }
    }

    if (tab === 1) setContent(searchCollectionState.data);

    return () => {};
  }, [searchNftState, searchCollectionState]);

  // Handle

  // Handle-pagination
  const onChangePage = (page: number, pageSize: number) => {
    dispatch(nftQueryAction({ limit: pageSize || 9, page: page }));
  };

  // Handle Search NFT
  const handleSearchCollection = (value: string) => {
    if (value) {
      dispatch(collectionQueryAction({ name: value }));
    }
    if (!value) {
      if (tab === 1) setContent(collectionState.data);
    }
  };

  const handleSearchNft = (value: string) => {
    if (value) {
      dispatch(nftSearchAction({ name: value, limit: 9, page: 1 }));
    }

    if (!value) {
      if (tab === 0) {
        dispatch(getNftByName(['not value']));
      }
    }
  };

  // Handle Click Card
  const handleClickCardNft = (nftId: string) => {
    dispatch(getNftByIdAction(nftId));

    setTimeout(() => {
      history.push(Epath.nftPage);
    }, 1500);
  };

  const handleClickCardCollection = (collectionId: string) => {
    dispatch(nftQueryAction({ collectionNft: collectionId, limit: 12 }));
    setTimeout(() => {
      history.push(Epath.collectionPage);
    }, 1500);
  };

  return (
    <>
      <Container>
        <SectionHeadline
          heading="Browse Marketplace"
          subHead="Browse through more than 50k NFTs on the NFT Marketplace."
        />

        {tab === 1 ? (
          <SearchBox
            placeholder="Search your favourite Collection"
            onClickSearch={handleSearchCollection}
          />
        ) : (
          <SearchBox placeholder="Search your favourite NFTs" handleSearch={handleSearchNft} />
        )}
      </Container>
      <Container paddingDefault={false}>
        <TabBar
          setTab={setTab}
          items={[
            {
              label: 'NFTs',
              quantity: totalResults,
            },
            {
              label: 'Collections',
              quantity: collectionState.totalResults,
            },
          ]}
        />
      </Container>
      <div
        className="secondary-background"
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
        }}
      >
        <Container>
          <Row gutter={[30, 30]}>
            {content && content.length > 0 ? (
              content.map((item: any, index: number) => {
                const createdDate = formatDate(item.createdAt);

                return (
                  <Col key={index} xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
                    {tab === 0 && (
                      <Card
                        type="discover"
                        cardBg="primary-background"
                        imagePlaceholder={item.image}
                        cardInfo={
                          <CardInfo
                            info={{
                              cardName: item.name,
                              avatar: item.user?.avatar,
                              userName: item.user?.name,
                            }}
                          />
                        }
                        artistInfo={[
                          {
                            title: 'Price',
                            total: `${item.price} ETH`,
                          },
                          {
                            title: 'Created Date',
                            total: createdDate,
                          },
                        ]}
                        onClick={() => handleClickCardNft(item.id)}
                      />
                    )}
                    {tab === 1 && (
                      <Card
                        key={index}
                        type="collection"
                        imagePlaceholder={item.image}
                        cardInfo={
                          <CardInfo
                            info={{
                              cardName: item.name,
                              description: item.description,
                            }}
                          />
                        }
                        listImage={{
                          size: 100,
                          listNfts: item?.nfts,
                        }}
                        onClick={() => handleClickCardCollection(item.id)}
                      />
                    )}
                  </Col>
                );
              })
            ) : (
              <h5>No data found, please enter complete data</h5>
            )}
            {searchNftState[0] !== 'not result' ? (
              <Col span={12} offset={6}>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                  <PaginationCustomize total={totalResults} onChangePage={onChangePage} />
                </div>
              </Col>
            ) : null}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Marketplace;
