import React, { useState, useEffect } from 'react';
import { Col, Row, Image } from 'antd';
import { GiftOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { ArrowRight } from 'app/components/Icons/svg';
import {
  buyNftAction,
  getNftByIdAction,
  giveNftAction,
  nftQueryAction,
  sellNftAction,
  updateNftAction,
} from 'utils/@reduxjs/slice/nftSlice';
import { IndexedObject } from 'types/common';
import { formatDate, formatTime } from 'app/helpers/functions';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import { uploadImageToFirebase } from 'app/helpers/uploadImage';
import ArtistCardSize from 'app/components/Common/Card/ArtistCard/ArtistCardSize';
import Container from 'app/components/Common/Form/Container';
import List from 'app/components/Common/List/List';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import AuctionTimer from 'app/components/Common/UI/AuctionTimer/AuctionTimer';
import Button from 'app/components/Common/UI/Button/Button';
import Card from 'app/components/Common/Card/Card';
import CardInfo from 'app/components/Common/Card/CardInfo/CardInfo';
import ImageBox from 'app/components/Common/UI/ImageBox/ImageBox';
import ModalCustomize from 'app/components/Common/UI/Modal/ModalCustomize';
import ButtonGroup from 'app/components/Common/UI/ButtonGroup/ButtonGroup';
import Input from 'app/components/Common/UI/Input/Input';
import Textarea from 'app/components/Common/UI/Textarea/Textarea';
import PaginationCustomize from 'app/components/Common/Pagination/PaginationCustomize';

type Props = {};

const imagePlaceholder =
  'https://www.myrelatedlife.com/app/uploads/2022/01/NFT-1-copy-1196x445-1300x445-c-default-1300x445-c-default.jpg';

const NFTPage = (props: Props) => {
  // Redux
  const dispatch = useDispatch();
  const stateNfts = useSelector((state: RootState) => state.nfts);

  const dataNft = stateNfts.dataNftById;
  const dataNftsByUser = stateNfts.dataNftByUser;
  const dataNftArtist = stateNfts.dataNftByUser.results;

  // Hooks
  const [dataNftById, setDataNftById] = useState(dataNft);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [labelModal, setLabelModal] = useState<string>('');

  const [isOnEdit, setIsOnEdit] = useState<boolean>(false);
  const [receiverId, setReceiverId] = useState<string>('');
  const [valueImage, setValueImage] = useState<IndexedObject>({ image: dataNftById.image });
  const [valueInput, setValueInput] = useState<IndexedObject>({
    name: dataNftById.name,
    image: dataNftById.image,
    price: dataNftById.price,
    description: dataNftById.description,
    collectionNft: dataNftById.collectionNft.id,
    tags: ['tag1', 'tag2'],
  });

  useEffect(() => {
    window.scrollTo(0, 0);

    dispatch(nftQueryAction({ user: dataNftById.user.id, limit: 9 }));

    return () => {};
  }, []);

  useEffect(() => {
    setDataNftById(dataNft);

    return () => {};
  }, [dataNft]);

  useEffect(() => {
    setValueImage({ image: dataNftById.image });
    setValueInput({
      name: dataNftById.name,
      image: dataNftById.image,
      price: dataNftById.price,
      description: dataNftById.description,
      collectionNft: dataNftById.collectionNft.id,
      tags: ['tag1', 'tag2'],
    });
    return () => {};
  }, [dataNftById]);

  // Contants
  const currentUser = getUserFromLocalStorage();
  const mintedDate = new Date(dataNftById.createdAt).toDateString().split(' ').slice(1).join(' ');
  const pricePreOrder = (dataNftById.price - dataNftById.price * (20 / 100)).toFixed(2);

  // Handle

  // Handle-Click
  const handleOnClick = (e: any) => {
    const innerText = e.currentTarget.innerText;

    switch (innerText) {
      case 'Sell NFT':
        setLabelModal('Sell NFT');
        setIsModalOpen(true);
        break;

      case 'Giveaways':
        setLabelModal('Giveaways');
        setIsModalOpen(true);
        break;

      case 'Pre-order NFT':
        // console.log('Pre-order NFT');
        break;

      case 'Edit':
        setIsOnEdit(true);
        break;

      case 'Save':
        handleUpdateNft();
        setIsOnEdit(false);
        break;

      case 'Cancel':
        setIsOnEdit(false);
        setValueImage({ image: dataNftById.image });
        setValueInput({
          name: dataNftById.name,
          price: dataNftById.price,
          description: dataNftById.description,
          tags: ['tag1', 'tag2'],
        });
        break;

      default:
        break;
    }
  };

  const onClickBuyOrCancelNft = (isSubmit: string) => {
    if (isSubmit === 'Place Bid') {
      dispatch(buyNftAction(dataNftById.id));
    } else {
      dispatch(sellNftAction(dataNftById.id));
    }
  };

  const onClickCard = (item: any) => {
    setDataNftById(item);

    window.scrollTo({
      top: 680,
      behavior: 'smooth',
    });
  };

  // Handle-Modal
  const onCancel = () => {
    setIsModalOpen(false);
    setLabelModal('');
    setReceiverId('');
  };

  const handleSubmit = (isSubmit: string) => {
    switch (isSubmit) {
      case 'Sell NFT':
        dispatch(sellNftAction(dataNftById.id));

        setIsModalOpen(false);
        setLabelModal('');
        break;

      case 'Giveaways':
        dispatch(giveNftAction({ nftId: dataNftById.id, receiverId: receiverId }));

        setIsModalOpen(false);
        setLabelModal('');
        setReceiverId('');
        break;

      default:
        break;
    }
  };

  // Handle-Pagination
  const onChangePage = (page: number, pageSize: number) => {
    dispatch(nftQueryAction({ user: dataNftById.user.id, limit: pageSize || 9, page: page }));
  };

  // Handle-Update
  const onChangeInput = (e: any, key: string) => {
    const value = e.currentTarget.value;
    setValueInput((prev) => ({ ...prev, [key]: value }));
  };

  const handleEditImage = async (e: any) => {
    const nameImage = e.target.files[0];
    const res = await uploadImageToFirebase(nameImage);

    if (res?.status === 'ok') {
      setValueImage({ image: res.url });
    }
  };

  const handleUpdateNft = () => {
    dispatch(
      updateNftAction({
        id: dataNftById.id,
        data: {
          name: valueInput.name,
          image: valueImage.image,
          price: valueInput.price,
          description: valueInput.description,
          collectionNft: dataNftById.collectionNft.id,
          tags: valueInput.tags,
        },
      }),
    );
  };

  // UI
  const dataListInfo = [
    {
      head: 'Price',
      items: [
        isOnEdit ? (
          <Input
            disabled={!isOnEdit}
            name="price"
            typeInput="number"
            value={valueInput.price}
            onChangeInput={onChangeInput}
          />
        ) : (
          `${dataNftById.price} ETH`
        ),
      ],
    },
    {
      head: 'Created By',
      items: [<ArtistCardSize avatar={dataNftById.user.avatar} userName={dataNftById.user.name} />],
    },
    {
      head: 'Description',
      items: [
        <Textarea
          disabled={!isOnEdit}
          name="description"
          value={valueInput.description}
          onChangeInput={onChangeInput}
        />,
      ],
    },
    {
      head: 'Details',
      items: ['View on Etherscan', 'View Original'],
    },
    {
      head: 'Tags',
      items: [
        // <Input disabled={!isOnEdit} name="tags" value="tags" onChangeInput={onChangeInput} />,
        'tags',
      ],
    },
  ];

  return (
    <>
      <ModalCustomize
        labelModal={labelModal}
        isOpen={isModalOpen}
        onCancel={onCancel}
        onSubmit={handleSubmit}
      >
        {labelModal === 'Sell NFT' && (
          <div style={{ display: 'grid', gap: 26 }}>
            <ImageBox size={{ width: 200, height: 200 }} url={dataNftById.image} />
            <h3>Name: {dataNftById.name}</h3>
            <h4>Price: {dataNftById.price} ETH</h4>
          </div>
        )}
        {labelModal === 'Giveaways' && (
          <>
            <div style={{ display: 'flex', gap: 16, marginBottom: 26 }}>
              <ImageBox size={{ width: 200, height: 200 }} url={dataNftById.image} />
              <div>
                <h5 style={{ marginBottom: 16 }}>Name: {dataNftById.name}</h5>
                <h5>Price: {dataNftById.price} ETH</h5>
              </div>
            </div>
            <div>
              <h5 style={{ marginBottom: 16 }}>Receiver ID</h5>
              <Input
                name="receiverId"
                value={receiverId}
                onChangeInput={(e: any) => setReceiverId(e.currentTarget.value)}
              />
            </div>
          </>
        )}
      </ModalCustomize>

      <div style={{ width: '100%', height: 560, position: 'relative', overflow: 'hidden' }}>
        <img
          src={imagePlaceholder}
          alt=""
          style={{ position: 'absolute', width: '100%', top: '0px' }}
        />
      </div>
      <Container>
        <Row>
          <Col span={16}>
            <div>
              <SectionHeadline
                heading={
                  isOnEdit ? (
                    <Input
                      disabled={!isOnEdit}
                      name="name"
                      value={valueInput.name}
                      onChangeInput={onChangeInput}
                    />
                  ) : (
                    dataNftById.name
                  )
                }
                subHead={`Minted on ${mintedDate}`}
              />
              <div style={{ display: 'grid', gap: 30, marginBottom: 80 }}>
                {dataListInfo.map((info, index) => (
                  <List key={index} type="secondary" head={info.head} items={info.items} />
                ))}
              </div>
              {currentUser && currentUser.id === dataNftById.user.id ? (
                <>
                  {isOnEdit ? (
                    <ButtonGroup
                      items={[
                        {
                          label: 'Save',
                          variant: 'secondary',
                          onClick: handleOnClick,
                        },
                        {
                          label: 'Cancel',
                          variant: 'secondary',
                          type: 'outline',
                          onClick: handleOnClick,
                        },
                      ]}
                    />
                  ) : (
                    <Button variant="secondary" onClick={handleOnClick}>
                      Edit
                    </Button>
                  )}
                </>
              ) : null}
            </div>
          </Col>
          <Col span={8}>
            <div style={{ display: 'grid', justifyContent: 'end', gap: 30 }}>
              <ImageBox
                edit={isOnEdit}
                size={{ width: 300, height: 300 }}
                url={valueImage.image}
                onChangeImage={handleEditImage}
              />
              {currentUser ? (
                <>
                  {dataNftById.status === 'mint' ? (
                    <>
                      <Button className="width-full" variant="secondary" onClick={handleOnClick}>
                        {currentUser.id === dataNftById.user.id
                          ? 'Sell NFT'
                          : `Pre-order ${pricePreOrder} ETH`}
                      </Button>
                      <Button
                        className="width-full"
                        variant="secondary"
                        type="outline"
                        icon={<GiftOutlined />}
                        onClick={handleOnClick}
                      >
                        Giveaways
                      </Button>
                    </>
                  ) : null}
                  {dataNftById.status === 'sell' ? (
                    <AuctionTimer
                      button
                      labelButton={
                        currentUser.id === dataNftById.user.id ? 'Cancel Sell' : 'Place Bid'
                      }
                      onClick={onClickBuyOrCancelNft}
                    />
                  ) : null}
                </>
              ) : (
                <Button className="width-full" variant="secondary" type="outline">
                  Login To Buy
                </Button>
              )}
            </div>
          </Col>
        </Row>
      </Container>
      <Container>
        <SectionHeadline
          heading="More from this artist"
          type="headline-button"
          button={
            <Button
              variant="secondary"
              type="outline"
              icon={<ArrowRight color="#A259FF" size={20} />}
            >
              Go To Artist Page
            </Button>
          }
        />
        <Row gutter={[30, 60]}>
          {dataNftArtist &&
            dataNftArtist.length > 0 &&
            dataNftArtist?.map((item: any, index: number) => {
              const createdDate = formatDate(item.createdAt);
              return (
                <Col key={index} xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
                  <Card
                    type="discover"
                    imagePlaceholder={item.image}
                    cardInfo={
                      <CardInfo
                        info={{
                          cardName: item.name,
                          avatar: item.user.avatar,
                          userName: item.user.name,
                        }}
                      />
                    }
                    artistInfo={[
                      {
                        title: 'Price',
                        total: `${item.price} ETH`,
                      },
                      {
                        title: 'Created Date',
                        total: createdDate,
                      },
                    ]}
                    onClick={() => onClickCard(item)}
                  />
                </Col>
              );
            })}

          <Col span={12} offset={6}>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <PaginationCustomize
                total={dataNftsByUser.totalResults}
                onChangePage={onChangePage}
              />
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default NFTPage;
