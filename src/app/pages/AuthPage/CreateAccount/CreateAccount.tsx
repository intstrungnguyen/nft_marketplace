import React from 'react';
import { Col, Row } from 'antd';
import { User, EnvelopeSimple, LockKey } from 'app/components/Icons/svg';
import { TAccount } from 'types/common';
import { useDispatch, useSelector } from 'react-redux';
import { registerAction } from 'utils/@reduxjs/slice/authSlice';
import { Epath } from 'app/routes/routesConfig';
import { setLoading } from 'utils/@reduxjs/slice/progressSlice';
import ImageBackground from '../../../images/background/ImagePlaceholder.jpg';
import FormInput from 'app/components/Common/Form/FormInput/FormInput';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import useResponsive from 'app/responsive/useResponsive';

type Props = {};

const itemsInput = [
  {
    type: 'text',
    name: 'name',
    placeholder: 'Username',
    icon: <User size={20} color="#BDBDBD" />,
  },
  {
    type: 'email',
    name: 'email',
    placeholder: 'Email Address',
    icon: <EnvelopeSimple size={20} color="#BDBDBD" />,
  },
  {
    type: 'password',
    name: 'password',
    placeholder: 'Password',
    icon: <LockKey size={20} color="#BDBDBD" />,
  },
  {
    type: 'password',
    name: 'confirmPassword',
    placeholder: 'Confirm Password',
    icon: <LockKey size={20} color="#BDBDBD" />,
  },
];

const CreateAccount: React.FC<Props> = (props: Props) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  const handleSubmit = async (data: TAccount) => {
    dispatch(registerAction({ data: data, navigate: Epath.loginPage }));
    dispatch(setLoading(false));
  };

  return (
    <Row align={'middle'}>
      <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
        <img src={ImageBackground} alt="" style={{ width: '100%', height: '100%' }} />
      </Col>
      <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
        <div style={{ padding: scrXl ? '0px 60px' : scrMd ? '0px 40px' : '30px 40px 40px 40px' }}>
          <SectionHeadline
            heading="Create account"
            subHead="Welcome! enter your details and start creating, collecting and selling NFTs."
          />
          <FormInput labelBtn="Create Account" items={itemsInput} handleSubmit={handleSubmit} />
        </div>
      </Col>
    </Row>
  );
};

export default CreateAccount;
