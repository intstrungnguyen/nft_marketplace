import React, { useEffect } from 'react';
import { Col, Row } from 'antd';
import { useHistory } from 'react-router-dom';
import { EnvelopeSimple, LockKey } from 'app/components/Icons/svg';
import { TAccount } from 'types/common';
import { RootState } from 'types/RootState';
import { useDispatch, useSelector } from 'react-redux';
import { loginAction } from 'utils/@reduxjs/slice/authSlice';
import { setLoading } from 'utils/@reduxjs/slice/progressSlice';
import ImageBackground from '../../../images/background/ImagePlaceholder.jpg';
import FormInput from 'app/components/Common/Form/FormInput/FormInput';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import useResponsive from 'app/responsive/useResponsive';

const itemsInput = [
  {
    type: 'email',
    name: 'email',
    placeholder: 'Email Address',
    icon: <EnvelopeSimple size={20} color="#BDBDBD" />,
  },
  {
    type: 'password',
    name: 'password',
    placeholder: 'Password',
    icon: <LockKey size={20} color="#BDBDBD" />,
  },
];

type Props = {};

const LoginPage: React.FC<Props> = (props: Props) => {
  // Redux
  const dispatch = useDispatch();
  const stateAuth = useSelector((state: RootState) => state.auth);

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const history = useHistory();

  useEffect(() => {
    if (stateAuth.authenticated === true) history.push('/');
  }, [stateAuth]);

  // Handle Submit
  const handleSubmit = (data: TAccount) => {
    dispatch(loginAction({ data: data }));
    dispatch(setLoading(false));
  };

  return (
    <Row align={'middle'}>
      <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
        <img src={ImageBackground} alt="" style={{ width: '100%', height: '100%' }} />
      </Col>
      <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
        <div style={{ padding: scrXl ? '0px 60px' : scrMd ? '0px 40px' : '30px 40px 40px 40px' }}>
          <SectionHeadline
            heading="Login"
            subHead="Welcome! enter your details and start creating, collecting and selling NFTs."
          />
          <FormInput labelBtn="Login" items={itemsInput} handleSubmit={handleSubmit} />
          <span style={{ display: 'block', marginTop: 26 }}>
            Do not have an account ?{' '}
            <a style={{ color: '#a259ff' }} onClick={() => history.push('/create-account')}>
              Create Account
            </a>
          </span>
        </div>
      </Col>
    </Row>
  );
};

export default LoginPage;
