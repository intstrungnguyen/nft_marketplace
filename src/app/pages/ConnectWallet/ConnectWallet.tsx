import React from 'react';
import { Col, Row } from 'antd';
import { User, EnvelopeSimple, LockKey } from 'app/components/Icons/svg';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import useResponsive from 'app/responsive/useResponsive';
import Button from 'app/components/Common/UI/Button/Button';
import Metamask from 'app/components/Icons/Logo/Metamask';
import WalletConnect from 'app/components/Icons/Logo/WalletConnect';
import Coinbase from 'app/components/Icons/Logo/Coinbase';

import { useSelector } from 'react-redux';

type Props = {};

const backgroundImage =
  'https://s3-alpha-sig.figma.com/img/0b34/7e10/5d353eae4c7b063b0da73eddb78c3a89?Expires=1712534400&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=FgtSNijuFiL2bPEv~yHkIbDATxS~wwvR5-ETeXShZvzHGSaF8RDfxPjmNoA8X6OrDnMQZWgo08CL7ScvG4MsqidNp6OGQxyWtDRTp1uNY5HWPkubJNLbIcxUkmMVbEuGCjNNtAouFV~QsPXwYTKEQrdKuBhsHrQhT3iYDS45RkfIHAl1D~6hWujgMo0B3oDSGh2vsk19AT7uYgzYgrgfTPRF1HD1fDoQ-kM7JVtrVuaqUSBpa4eO1l7Fkjqd6tftXN399Ud2AvgFKha~bZ2vVitmQT36EhSnHBBOiJhZ3j7VKaXfx5LWOdol1D1tgOAHNlX0jRsuP5vic93AUNJf4Q__';

const listBtn = [
  {
    label: 'Metamask',
    icon: <Metamask size={40} />,
  },
  {
    label: 'Wallet Connect',
    icon: <WalletConnect size={40} />,
  },
  {
    label: 'Coinbase',
    icon: <Coinbase size={40} />,
  },
];
const ConnectWallet: React.FC<Props> = (props: Props) => {
  const { scrXl, scrMd, scrXs } = useResponsive();
  const state = useSelector((state) => state);

  return (
    <Row align={'middle'}>
      <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
        <img src={backgroundImage} alt="" style={{ width: '100%', height: '100%' }} />
      </Col>
      <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
        <div style={{ padding: scrXl ? '0px 60px' : scrMd ? '0px 40px' : '30px 40px 40px 40px' }}>
          <SectionHeadline
            heading="Connect wallet"
            subHead="Choose a wallet you want to connect. There are several wallet providers."
          />
          <div style={{ display: 'grid', gap: '20px', width: 330 }}>
            {listBtn.map((item, index) => (
              <Button
                key={index}
                variant="primary"
                type="outline"
                className="width-full secondary-background"
                justify="none"
                icon={item.icon}
              >
                {item.label}
              </Button>
            ))}
          </div>
        </div>
      </Col>
    </Row>
  );
};

export default ConnectWallet;
