import React from 'react';
import { Row, Col } from 'antd';
import Card from 'app/components/Common/Card/Card';
import Headline from 'app/components/Common/Headline/Headline';
import useResponsive from 'app/responsive/useResponsive';
import { GroupIconWallet, GroupIconCollection, GroupIconCart } from 'app/components/Icons/svg';

type Props = {
  // dataWork: {
  //   icon: React.ReactNode;
  //   headline: string;
  //   subHead: string;
  // }[];
};

const HowItWorks: React.FC<Props> = ({}) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  const dataWork = [
    {
      icon: <GroupIconWallet size={scrXl ? undefined : scrMd ? 160 : 100} />,
      headline: 'Setup Your wallet',
      subHead:
        'Set up your wallet of choice. Connect it to the Animarket by clicking the wallet icon in the top right corner.',
    },
    {
      icon: <GroupIconCollection size={scrXl ? undefined : scrMd ? 160 : 100} />,
      headline: 'Create Collection',
      subHead:
        'Upload your work and setup your collection. Add a description, social links and floor price.',
    },
    {
      icon: <GroupIconCart size={scrXl ? undefined : scrMd ? 160 : 100} />,
      headline: 'Start Earning',
      subHead:
        'Choose between auctions and fixed-price listings. Start earning by selling your NFTs or trading others.',
    },
  ];

  return (
    <>
      <Row className="items-center" gutter={scrXs ? [20, 20] : [30, 30]}>
        {dataWork &&
          dataWork.length > 0 &&
          dataWork.map((work, index) => (
            <Col key={index} xl={{ span: 8 }} md={{ span: 8 }} xs={{ span: 24 }}>
              <Card
                type="work"
                imagePlaceholderComp={work.icon}
                cardInfo={<Headline type="card" headline={work.headline} subHead={work.subHead} />}
              />
            </Col>
          ))}
      </Row>
    </>
  );
};

export default HowItWorks;
