import React from 'react';
import { Row, Col } from 'antd';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RocketLaunch } from 'app/components/Icons/svg';
import HeroAnimation from '../../../../images/Gif/heroanimation.gif';
import Headline from 'app/components/Common/Headline/Headline';
import Button from 'app/components/Common/UI/Button/Button';
import AdditionalInfo from 'app/components/Common/AdditionalInfo/AdditionalInfo';
import useResponsive from 'app/responsive/useResponsive';
import { RootState } from 'types/RootState';

type Props = {};

type TOverview = {
  totalSaleEth: number;
  quantitySaleNfts: number;
  quantityArtists: number;
};

const HeroSection: React.FC<Props> = () => {
  // Redux
  const state = useSelector((state: RootState) => state);
  const overviewState: TOverview = state.common.dataOvervier;

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const history = useHistory();

  // Contants
  const items = [
    {
      title: `${parseFloat((Math.floor(overviewState.totalSaleEth) / 1000).toFixed(1))}K+`,
      content: 'Total Sale',
    },
    {
      title: `${overviewState.quantitySaleNfts}`,
      content: 'Auctions',
    },
    {
      title: `${overviewState.quantityArtists}`,
      content: 'Artists',
    },
  ];

  return (
    <>
      <Row className="items-center" gutter={scrXs ? [40, 40] : [30, 30]}>
        <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
          <Headline
            headline="Discover Digital Art & Collect NFTs"
            subHead="NFT Marketplace UI Created With Anima For Figma. Collect, Buy And Sell Art From More Than
              20k NFT Artists."
          />

          {scrXs ? null : (
            <>
              <div style={{ padding: scrMd ? '20px 0px' : '40px 0px' }}>
                <Button variant="secondary" icon={<RocketLaunch size={20} />}>
                  Get Started
                </Button>
              </div>
              <AdditionalInfo items={items} />
            </>
          )}
        </Col>
        <Col xl={{ span: 12 }} md={{ span: 12 }} xs={{ span: 24 }}>
          <div style={{ cursor: 'pointer' }} onClick={() => history.push('/artist-page')}>
            <img src={HeroAnimation} alt="" />
          </div>
          {/* <Card
              type="normal"
              imagePlaceholder={HeroAnimation}
              cardInfo={
                <CardInfo
                  info={{
                    cardName: 'Space Walking',
                    avatar: Avatar_14,
                    userName: 'Animakid',
                  }}
                />
              }
            /> */}
        </Col>
      </Row>
      {scrXs && (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col xs={{ span: 24 }}>
            <div style={{ padding: '40px 0px' }}>
              <Button
                className={scrXs ? 'width-full' : ''}
                variant="secondary"
                icon={<RocketLaunch size={20} />}
              >
                Get Started
              </Button>
            </div>
            <AdditionalInfo items={items} />
          </Col>
        </Row>
      )}
    </>
  );
};

export default HeroSection;
