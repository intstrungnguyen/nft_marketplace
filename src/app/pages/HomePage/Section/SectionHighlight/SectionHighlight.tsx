import React from 'react';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import './SectionHighlight.scss';
import { useSelector, useDispatch } from 'react-redux';
import { getNftByIdAction } from 'utils/@reduxjs/slice/nftSlice';
import { RootState } from 'types/RootState';
import { Epath } from 'app/routes/routesConfig';
import { Eye } from 'app/components/Icons/svg';
import ArtistCardSize from 'app/components/Common/Card/ArtistCard/ArtistCardSize';
import Button from 'app/components/Common/UI/Button/Button';
import useResponsive from 'app/responsive/useResponsive';
import AuctionTimer from 'app/components/Common/UI/AuctionTimer/AuctionTimer';

type Props = {};

const SectionHighlight: React.FC<Props> = ({ children }) => {
  // State Redux
  const dispatch = useDispatch();
  const stateNfts = useSelector((state: RootState) => state.nfts);
  const nftHighlight = stateNfts.nftHighlight;
  const dataUser = nftHighlight.user;

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const history = useHistory();

  // Handle
  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  // Handle Click
  const handleOnClick = () => {
    dispatch(getNftByIdAction(nftHighlight.id));

    setTimeout(() => {
      history.push(Epath.nftPage);
    }, 1500);
  };

  return (
    <div className={clsx('section-highlight', screens)}>
      <div style={{ display: 'grid', gap: 30 }}>
        <ArtistCardSize avatar={dataUser?.avatar} userName={dataUser?.name} />
        <h2>{nftHighlight.name}</h2>

        {scrXs ? null : (
          <Button
            className="white"
            variant="secondary"
            icon={<Eye color="#A259FF" size={20} />}
            onClick={handleOnClick}
          >
            See NFT
          </Button>
        )}
      </div>
      <AuctionTimer />
      {scrXs && (
        <Button
          className="white width-full"
          variant="secondary"
          icon={<Eye color="#A259FF" size={20} />}
          onClick={handleOnClick}
        >
          See NFT
        </Button>
      )}
    </div>
  );
};

export default SectionHighlight;
