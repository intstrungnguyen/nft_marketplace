import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'types/RootState';
import { formatDate } from 'app/helpers/functions';
import { getNftByIdAction } from 'utils/@reduxjs/slice/nftSlice';
import { Epath } from 'app/routes/routesConfig';
import useResponsive from 'app/responsive/useResponsive';
import Card from 'app/components/Common/Card/Card';
import CardInfo from 'app/components/Common/Card/CardInfo/CardInfo';

type Props = {};

type TDiscover = {
  id: string;
  name: string;
  image: string;
  user: any;
  price: number;
  createdAt: string;
  artistInfo: {
    title: string;
    total: string | number;
  }[];
}[];

const DiscoverMore: React.FC<Props> = (props: Props) => {
  // State Redux
  const dispatch = useDispatch();
  const stateNfts = useSelector((state: RootState) => state.nfts);
  const dataDiscover = stateNfts.dataNFTsLimit;

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const history = useHistory();
  const [discover, setDiscover] = useState<TDiscover>();

  useEffect(() => {
    if (scrXl || scrXs) {
      setDiscover(dataDiscover?.slice(0, 3));
    }
    if (scrMd) {
      setDiscover(dataDiscover?.slice(0, 2));
    }

    return () => {};
  }, [scrXl, scrMd, scrXs, dataDiscover]);

  // Handle
  const onCardClick = (id: string) => {
    dispatch(getNftByIdAction(id));

    setTimeout(() => {
      history.push(Epath.nftPage);
    }, 1500);
  };

  return (
    <>
      <Row className="items-center" gutter={scrXs ? [20, 20] : [30, 30]}>
        {dataDiscover &&
          dataDiscover.length > 0 &&
          discover?.map((discover, index) => {
            const createdDate = formatDate(discover.createdAt);
            return (
              <Col key={index} xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
                <Card
                  type="discover"
                  imagePlaceholder={discover.image}
                  cardInfo={
                    <CardInfo
                      info={{
                        cardName: discover.name,
                        avatar: discover.user.avatar,
                        userName: discover.user.name,
                      }}
                    />
                  }
                  artistInfo={[
                    {
                      title: 'Price',
                      total: `${discover.price} ETH`,
                    },
                    {
                      title: 'Created Date',
                      total: createdDate,
                    },
                  ]}
                  onClick={() => onCardClick(discover.id)}
                />
              </Col>
            );
          })}
      </Row>
    </>
  );
};

export default DiscoverMore;
