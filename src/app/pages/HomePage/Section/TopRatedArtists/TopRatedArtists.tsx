import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';
import { Row, Col } from 'antd';
import { RootState } from 'types/RootState';
import { useSelector, useDispatch } from 'react-redux';
import ArtistCard from 'app/components/Common/Card/ArtistCard/ArtistCard';
import ArtistInfo from 'app/components/Common/Card/ArtistInfo/ArtistInfo';
import useResponsive from 'app/responsive/useResponsive';
import { getUserByIdAction } from 'utils/@reduxjs/slice/userSlice';

type Props = {};

type TArtist = {
  id: string;
  name: string;
  avatar: string;
  totalPrice: number;
}[];

const TopRatedArtists: React.FC<Props> = (props: Props) => {
  // State Redux
  const dispatch = useDispatch();
  const artistState = useSelector((state: RootState) => state.user);
  const dataArtist = artistState.dataUserRanking;

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const [artist, setArtist] = useState<TArtist>();
  const history = useHistory();

  useEffect(() => {
    if (scrXl) {
      setArtist(dataArtist?.slice(0, 4));
    }
    if (scrMd) {
      setArtist(dataArtist?.slice(0, 6));
    }
    if (scrXs) {
      setArtist(dataArtist?.slice(0, 6));
    }

    return () => {};
  }, [scrXl, scrMd, scrXs, dataArtist]);

  // Handle

  const onClickCard = (id: string) => {
    dispatch(getUserByIdAction(id));
    setTimeout(() => {
      history.push(Epath.artistPage);
    }, 1000);
  };

  return (
    <>
      <Row className="items-center" gutter={scrXs ? [20, 20] : [30, 30]}>
        {dataArtist &&
          dataArtist.length > 0 &&
          artist?.map((artist, index) => (
            <Col key={index} xl={{ span: 6 }} md={{ span: 12 }} xs={{ span: 24 }}>
              <ArtistCard
                avatar={artist.avatar}
                info={
                  <ArtistInfo
                    name={artist.name}
                    title="Total Sales:"
                    total={Math.round(artist.totalPrice)}
                  />
                }
                rank={index + 1}
                onClickCard={() => onClickCard(artist.id)}
              />
            </Col>
          ))}
      </Row>
    </>
  );
};

export default TopRatedArtists;
