import React, { useEffect, useState } from 'react';
import { Row, Col, Skeleton } from 'antd';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'types/RootState';
import { Epath } from 'app/routes/routesConfig';
import { nftQueryAction } from 'utils/@reduxjs/slice/nftSlice';
import { collectionQueryAction } from 'utils/@reduxjs/slice/collectionSlice';
import ImageCollectionDefi from '../../../../images/collection/defi.jpg';
import ImageCollectionGameFi from '../../../../images/collection/game.jpg';
import ImageCollectionMeme from '../../../../images/collection/meme.jpg';
import Card from 'app/components/Common/Card/Card';
import CardInfo from 'app/components/Common/Card/CardInfo/CardInfo';
import useResponsive from 'app/responsive/useResponsive';
import nftApi from 'app/axios/api/nftApi';

type Props = {};

type TCollection = {
  id: string;
  name: string;
  description: string;
  image: string;
  quantityNfts: string;
  view: number;
  nfts: [];
}[];

const imagePlaceholder = [ImageCollectionDefi, ImageCollectionGameFi, ImageCollectionMeme];

const TrendingCollection: React.FC<Props> = (props: Props) => {
  // State Redux
  const dispatch = useDispatch();
  const stateCollection = useSelector((state: RootState) => state.collection);
  const dataCollection = stateCollection.dataCollectionQuery;
  const isLoading = stateCollection.isLoading;

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const history = useHistory();
  const [collections, setCollections] = useState<TCollection>();

  useEffect(() => {
    if (scrXl) {
      dispatch(collectionQueryAction({ limit: 3, sortBy: 'view:desc' }));
    }
    if (scrMd) {
      dispatch(collectionQueryAction({ limit: 2, sortBy: 'view:desc' }));
    }
    if (scrXs) {
      dispatch(collectionQueryAction({ limit: 1, sortBy: 'view:desc' }));
    }

    return () => {};
  }, [scrXl, scrMd, scrXs]);

  useEffect(() => {
    handleCombinedCollectionAndNft();

    return () => {};
  }, [dataCollection]);

  // Handle
  const handleCombinedCollectionAndNft = async () => {
    const combined = await Promise.all(
      dataCollection?.map(async (collection: any, index: number) => {
        const nftsResponse = await nftApi.getNFTQuery({ collectionNft: collection.id });
        return {
          ...collection,
          image: imagePlaceholder[index],
          nfts: nftsResponse.data.results,
        };
      }),
    );
    setCollections(combined);
  };

  const handleClickCard = (collectionId: string) => {
    dispatch(nftQueryAction({ collectionNft: collectionId, limit: 12 }));
    setTimeout(() => {
      history.push(Epath.collectionPage);
    }, 1500);
  };

  return (
    <>
      <Row className="items-center" gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        {collections?.map((collection, index: number) => (
          <Col key={index} xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
            <Card
              isLoading={isLoading}
              key={index}
              type="collection"
              imagePlaceholder={collection.image}
              cardInfo={
                <CardInfo
                  isLoading={isLoading}
                  info={{
                    cardName: collection.name,
                    description: collection.description,
                  }}
                />
              }
              listImage={{
                size: 100,
                listNfts: collection.nfts,
              }}
              onClick={() => handleClickCard(collection.id)}
            />
          </Col>
        ))}
      </Row>
    </>
  );
};

export default TrendingCollection;
