import React from 'react';
import './SubscribeWidget.scss';
import clsx from 'clsx';
import imageWidget from '../../../../images/background/background2.png';
import ImageBox from 'app/components/Common/UI/ImageBox/ImageBox';
import Headline from 'app/components/Common/Headline/Headline';
import SubscribeForm from 'app/components/Common/Form/SubscribeForm/SubscribeForm';
import Button from 'app/components/Common/UI/Button/Button';
import { EnvelopeSimple } from 'app/components/Icons/svg';
import useResponsive from 'app/responsive/useResponsive';

type Props = {};

const SubscribeWidget: React.FC<Props> = (props: Props) => {
  const { scrXl, scrMd, scrXs } = useResponsive();

  let screens;
  if (scrXl) screens = 'scrXl';
  if (scrMd) screens = 'scrMd';
  if (scrXs) screens = 'scrXs';

  return (
    <div className={clsx('subscribe-widget', screens)}>
      <ImageBox
        size={{
          width: scrXl ? 425 : scrMd ? 300 : 315,
          height: scrXl ? 310 : scrMd ? 280 : 255,
        }}
        url={imageWidget}
      />
      <div className={clsx('subscribe-widget-form', screens)}>
        <Headline
          type="form"
          headline="Join Our Weekly Digest"
          subHead="Get Exclusive Promotions & Updates Straight To Your Inbox."
        />
        <SubscribeForm
          type={scrXl ? 'nested' : 'vertical'}
          size={scrXl ? 'xl' : 'md'}
          input={{
            placeholder: 'Enter your email here',
          }}
          button={
            <Button
              className={scrXl ? '' : 'width-full'}
              variant={scrXl ? 'secondary' : 'tertiary'}
              icon={<EnvelopeSimple size={20} />}
            >
              Subscribe
            </Button>
          }
        />
      </div>
    </div>
  );
};

export default SubscribeWidget;
