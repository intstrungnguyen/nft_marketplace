import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Col, Skeleton } from 'antd';
import { RocketLaunch, Eye } from 'app/components/Icons/svg';
import useResponsive from 'app/responsive/useResponsive';

import Container from 'app/components/Common/Form/Container';
import Button from 'app/components/Common/UI/Button/Button';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import Highlight from 'app/components/Common/Highlight/Highlight';

import HeroSection from './Section/HeroSection/HeroSection';
import SubscribeWidget from './Section/SubscribeWidget/SubscribeWidget';
import TrendingCollection from './Section/TrendingCollection/TrendingCollection';
import TopRatedArtists from './Section/TopRatedArtists/TopRatedArtists';
import DiscoverMore from './Section/DiscoverMore/DiscoverMore';
import HowItWorks from './Section/HowItWorks/HowItWorks';
import SectionHighlight from './Section/SectionHighlight/SectionHighlight';

import { useDispatch, useSelector } from 'react-redux';
import { collectionQueryAction } from 'utils/@reduxjs/slice/collectionSlice';
import { nftQueryAction, nftHighlightAction } from 'utils/@reduxjs/slice/nftSlice';
import { userRankingAction } from 'utils/@reduxjs/slice/userSlice';
import { commonAction } from 'utils/@reduxjs/slice/commonSlice';
import { RootState } from 'types/RootState';
import { Epath } from 'app/routes/routesConfig';

type Props = {};

const HomePage: React.FC<Props> = (props: Props) => {
  // Redux
  const dispatch = useDispatch();
  const stateNfts = useSelector((state: RootState) => state.nfts);
  const nftHighlight = stateNfts.nftHighlight;

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const [bannerHighlight, setBannerHighlight] = useState<string>();
  const history = useHistory();

  useEffect(() => {
    dispatch(collectionQueryAction({ limit: 3, sortBy: 'view:desc' }));
    dispatch(userRankingAction());
    dispatch(nftQueryAction({ sortBy: 'createdAt:desc' }));
    dispatch(nftHighlightAction({ limit: 1, sortBy: 'view:desc' }));
    dispatch(commonAction());

    return () => {};
  }, []);

  useEffect(() => {
    setBannerHighlight(nftHighlight.image);
  }, [nftHighlight]);

  // Handle

  // Handle Click
  const handleOnClick = (e: any) => {
    const innerText = e.currentTarget.innerText;

    switch (innerText) {
      case 'View Rankings':
        history.push(Epath.rankingsPage);
        break;

      case 'See All':
        history.push(Epath.marketplacePage);
        break;

      default:
        break;
    }
  };

  return (
    <>
      <Container>
        <HeroSection />
      </Container>

      <Container>
        <SectionHeadline
          heading="Trending Collection"
          subHead="Checkout our weekly updated trending collection."
          button={
            <Button variant="secondary" type="outline" icon={<RocketLaunch size={20} />}>
              View Rankings
            </Button>
          }
        />
        <TrendingCollection />
      </Container>

      <Container>
        <SectionHeadline
          type="headline-button"
          heading="Top creators"
          subHead="Checkout Top Rated Creators on the NFT Marketplace"
          button={
            <Button
              variant="secondary"
              type="outline"
              icon={<RocketLaunch color="#A259FF" size={20} />}
              onClick={handleOnClick}
            >
              View Rankings
            </Button>
          }
        />
        <TopRatedArtists />
      </Container>

      <Container>
        <SectionHeadline
          type="headline-button"
          heading="Discover More NFTs"
          subHead="Explore new trending NFTs"
          button={
            <Button
              variant="secondary"
              type="outline"
              icon={<Eye color="#A259FF" size={20} />}
              onClick={handleOnClick}
            >
              See All
            </Button>
          }
        />
        <DiscoverMore />
      </Container>

      <Highlight backgroundImage={bannerHighlight ? bannerHighlight : ''}>
        <Container paddingDefault={false}>
          <SectionHighlight />
        </Container>
      </Highlight>

      <Container>
        <SectionHeadline heading="How it works" subHead="Find out how to get started" />
        <HowItWorks />
      </Container>

      <Container>
        <Row className="items-center" gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col xl={{ span: 24 }} md={{ span: 24 }} xs={{ span: 24 }}>
            <SubscribeWidget />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default HomePage;
