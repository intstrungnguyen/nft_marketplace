import React, { useState, useEffect } from 'react';
import { Row, Col, notification } from 'antd';
import { Copy, Globe, Plus } from 'app/components/Icons/svg';
import Highlight from 'app/components/Common/Highlight/Highlight';
import Container from 'app/components/Common/Form/Container';
import useResponsive from 'app/responsive/useResponsive';
import ImageBox from 'app/components/Common/UI/ImageBox/ImageBox';
import AdditionalInfo from 'app/components/Common/AdditionalInfo/AdditionalInfo';
import List from 'app/components/Common/List/List';
import DiscordLogo from 'app/components/Icons/Logo/DiscordLogo';
import YoutubeLogo from 'app/components/Icons/Logo/YoutubeLogo';
import TwitterLogo from 'app/components/Icons/Logo/TwitterLogo';
import InstagramLogo from 'app/components/Icons/Logo/InstagramLogo';
import ButtonGroup from 'app/components/Common/UI/ButtonGroup/ButtonGroup';
import TabBar from 'app/components/Common/UI/TabBar/TabBar';
import Card from 'app/components/Common/Card/Card';
import CardInfo from 'app/components/Common/Card/CardInfo/CardInfo';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'types/RootState';
import ModalCustomize from 'app/components/Common/UI/Modal/ModalCustomize';
import { getUserFromLocalStorage } from 'app/helpers/localStorage';
import { TUserDetail } from 'types/common';
import { updateUserAction } from 'utils/@reduxjs/slice/userSlice';
import { createNftAction, nftQueryAction } from 'utils/@reduxjs/slice/nftSlice';
import FormModal from 'app/components/Common/Form/FormModal/FormModal';
import { uploadImageToFirebase } from 'app/helpers/uploadImage';
import { formatDate } from 'app/helpers/functions';

type Props = {};

const items = [
  {
    title: '250k+',
    content: 'Volume',
  },
  {
    title: '50+',
    content: 'NFTs Sold',
  },
  {
    title: '3000+',
    content: 'Followers',
  },
];

const ArtistPage: React.FC = (props: Props) => {
  const dataCurrentUser: TUserDetail = getUserFromLocalStorage();

  // State Redux
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state);
  const artistState = state.user.dataUserById;
  const stateNft = state.nfts;

  const itemsInputUpdateUser = [
    {
      label: 'Name',
      name: 'name',
      placeholder: dataCurrentUser?.name,
    },
    {
      label: 'Email',
      name: 'email',
      placeholder: dataCurrentUser?.email,
    },
    {
      label: 'Bio',
      name: 'bio',
      placeholder: dataCurrentUser?.bio,
    },
  ];

  const itemsInputNft = [
    {
      label: 'NFT Image',
      name: 'image',
      typeInput: 'file',
    },
    {
      label: 'Name NFT',
      name: 'name',
    },
    {
      label: 'Description',
      name: 'description',
    },
    {
      label: 'Price',
      name: 'price',
      typeInput: 'number',
    },
  ];

  // Hooks
  const { scrXl, scrMd, scrXs } = useResponsive();
  const [tab, setTab] = useState<number>(0);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [labelModal, setLabelModal] = useState<string>('');
  const [contentModal, setContentModal] = useState<any[]>([]);
  const [formValue, setFormValue] = useState<{ [key: string]: any }>({});
  const [image, setImage] = useState<any>(null);

  useEffect(() => {
    window.scrollTo(0, 0);

    if (artistState) fetchDataNftCurrentUser();
    return () => {};
  }, []);

  const fetchDataNftCurrentUser = () => {
    dispatch(nftQueryAction({ user: artistState.id, sortBy: 'createdAt:desc' }));
  };

  // Handle

  // Handle-Modal
  const onCancel = () => {
    setFormValue({});
    setImage(null);
    setIsModalOpen(false);
  };

  const onClickEdit = (e: React.MouseEvent<HTMLButtonElement>) => {
    const innerText = e.currentTarget.innerText;

    switch (innerText) {
      case 'Edit Profile':
        if (dataCurrentUser) {
          setLabelModal(innerText);
          setIsModalOpen(true);
          setContentModal(itemsInputUpdateUser);
        }

        break;

      case 'Follow':
        // console.log('Follow');

        break;

      default:
        break;
    }
  };

  const onClickCreateNft = () => {
    setIsModalOpen(true);
    setLabelModal('Create NFT');
    setContentModal(itemsInputNft);
  };

  // Hanlde API
  const handleSubmit = async (isForm: string | undefined) => {
    switch (isForm) {
      case 'Edit Profile':
        dispatch(
          updateUserAction({
            name: formValue.name || dataCurrentUser.name,
            email: formValue.email || dataCurrentUser.email,
            avatar: formValue.avatar || dataCurrentUser.avatar,
            banner: formValue.banner || dataCurrentUser.banner,
            bio: formValue.bio || dataCurrentUser.bio,
          }),
        );

        break;

      case 'Create NFT':
        const res = await uploadImageToFirebase(image);

        if (res?.status === 'ok') {
          dispatch(
            createNftAction({
              name: formValue.name,
              image: res.url || '',
              collectionNft: formValue.collectionNft || '65f6ea46ca9244949b806d42',
              description: formValue.description,
              price: formValue.price,
              tags: ['tag1', 'tag2'],
            }),
          );
        }

        break;
      default:
        break;
    }

    if (!stateNft.error) {
      setIsModalOpen(false);
      setFormValue({});
      setImage(null);
      notification.success({
        message: 'Success',
      });
    }
  };

  return (
    <>
      {artistState ? (
        <>
          <ModalCustomize
            labelModal={labelModal}
            isOpen={isModalOpen}
            onCancel={onCancel}
            onSubmit={handleSubmit}
          >
            <FormModal
              items={contentModal}
              value={formValue}
              setValue={setFormValue}
              setImage={setImage}
            />
          </ModalCustomize>

          <Highlight backgroundImage={artistState.banner} type="cover-photo" />
          <Container paddingDefault={false}>
            <div style={{ position: 'absolute', top: -60 }}>
              <ImageBox
                size={{
                  width: 120,
                  height: 120,
                }}
                url={artistState.avatar}
                border={true}
              />
            </div>
          </Container>
          <Container>
            <Row gutter={[30, 30]}>
              <Col xl={{ span: 12 }} md={{ span: 24 }} xs={{ span: 24 }}>
                <h2>{artistState.name}</h2>
              </Col>
              <Col xl={{ span: 12 }} md={{ span: 24 }} xs={{ span: 24 }}>
                <div style={{ textAlign: 'end' }}>
                  <ButtonGroup
                    justify="end"
                    items={[
                      {
                        label: '0xc0E3...B79C',
                        variant: 'secondary',
                        icon: <Copy size={20} />,
                      },
                      {
                        label:
                          dataCurrentUser && artistState.id === dataCurrentUser?.id
                            ? 'Edit Profile'
                            : 'Follow',
                        variant: 'secondary',
                        type: 'outline',
                        icon: <Plus size={20} color="#A259FF" />,
                        onClick: (e) => onClickEdit(e),
                      },
                    ]}
                  />
                </div>
              </Col>
              <Col xl={{ span: 12 }} md={{ span: 24 }} xs={{ span: 24 }}>
                <div style={{ display: 'grid', gap: 30 }}>
                  <AdditionalInfo items={items} />
                  <List type="secondary" head="Bio" items={[`${artistState.bio}`]} />
                  <List
                    type="secondary"
                    head="Links"
                    contacts={[
                      <Globe size={32} color="#858584" />,
                      <DiscordLogo />,
                      <YoutubeLogo />,
                      <TwitterLogo />,
                      <InstagramLogo />,
                    ]}
                  />
                </div>
              </Col>
            </Row>
          </Container>
          <Container paddingDefault={false}>
            <TabBar
              setTab={setTab}
              items={[
                {
                  label: 'Created',
                  quantity: 302,
                },
                {
                  label: 'Owned',
                  quantity: 67,
                },
                {
                  label: 'Collection',
                  quantity: 4,
                },
              ]}
            />
          </Container>
          <div
            className="secondary-background"
            style={{ display: 'flex', justifyContent: 'center', width: '100%' }}
          >
            <Container>
              <Row gutter={[30, 30]}>
                {artistState.id === dataCurrentUser?.id ? (
                  <Col xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
                    <Card
                      type="create"
                      cardBg="primary-background"
                      onClick={() => onClickCreateNft()}
                    />
                  </Col>
                ) : null}
                {stateNft.dataNftCurrentUser &&
                  stateNft.dataNftCurrentUser.length > 0 &&
                  stateNft.dataNftCurrentUser?.map((item: any, index: number) => {
                    const createdDate = formatDate(item.createdAt);

                    return (
                      <Col key={index} xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
                        <Card
                          type="discover"
                          cardBg="primary-background"
                          imagePlaceholder={item.image}
                          cardInfo={
                            <CardInfo
                              info={{
                                cardName: item.name,
                                avatar: artistState.avatar,
                                userName: artistState.name,
                              }}
                            />
                          }
                          artistInfo={[
                            {
                              title: 'Price',
                              total: `${item.price} ETH`,
                            },
                            {
                              title: 'Created Date',
                              total: createdDate,
                            },
                          ]}
                        />
                      </Col>
                    );
                  })}
              </Row>
            </Container>
          </div>
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </>
  );
};

export default ArtistPage;
