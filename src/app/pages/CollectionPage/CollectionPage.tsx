import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'types/RootState';
import { formatDate } from 'app/helpers/functions';
import { getNftByName, nftQueryAction, nftSearchAction } from 'utils/@reduxjs/slice/nftSlice';
import Container from 'app/components/Common/Form/Container';
import SectionHeadline from 'app/components/Common/SectionHeadline/SectionHeadline';
import SearchBox from 'app/components/Common/SearchBox/SearchBox';
import Card from 'app/components/Common/Card/Card';
import CardInfo from 'app/components/Common/Card/CardInfo/CardInfo';
import ImageBox from 'app/components/Common/UI/ImageBox/ImageBox';

type Props = {};

const imagePlaceholder =
  'https://s3-alpha-sig.figma.com/img/a792/ddc0/c4e1193ffd08cf4918e6f696bbc8d8fe?Expires=1711929600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=R9YBHfNsprojgYHH2izkv08f3zJmAVTttidwdHleGmNWL05LDH9FfwgQ3OLS-m0WHPG8CWtvomCKJClGZXUGWZwCSotfgs2A4w~xHaIxo3RqdQZO6HujCO1EUaoTVj6XN-gBCfXZZbGVquVe~LBakdOJ0Prt0HsRuxo8j6p7XIqVhh9uEwOol86bP37hUuEG1TytA~9WYTEhffmDQ9oOwL~4ktFPKiW5o1FKJYgtWzhf--KUjktSTh4fKS5oKaznSS6aR4TP63C1CFsA6mJ2XzDETcUdAs95KIkVxzE2xGs5BBXfch6WtRCZAzxUgixev77bijP7ov1OOXw7NZPXZA__';

const CollectionPage = (props: Props) => {
  // Redux
  const dispatch = useDispatch();
  const state = useSelector((state: RootState) => state);
  const dataNFTsOfCollection = state.nfts.dataNFTsOfCollection;
  const searchNftState = state.nfts.dataSearchNFTs;

  // Hooks
  const [nftState, setNftState] = useState<any[]>(dataNFTsOfCollection.results);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    setNftState(dataNFTsOfCollection.results);
  }, [dataNFTsOfCollection]);

  useEffect(() => {
    if (searchNftState?.results?.length > 0 && searchNftState[0] !== 'not value') {
      setNftState(searchNftState.results);
    }

    if (searchNftState[0] === 'not value') {
      setNftState(dataNFTsOfCollection.results);
    }

    if (searchNftState[0] === 'not result') {
      setNftState([]);
    }

    return () => {};
  }, [searchNftState]);

  // Handle

  // Handle-Search
  const handleSearchNft = (value: string) => {
    if (value) {
      dispatch(
        nftSearchAction({
          collectionNft: nftState[0].collectionNft.id,
          name: value,
          limit: 12,
        }),
      );
    }

    if (!value) {
      dispatch(getNftByName(['not value']));
    }
  };

  return (
    <>
      <Container>
        <div style={{ display: 'flex', gap: 16, marginBottom: 60 }}>
          <ImageBox
            size={{ width: 160, height: 160 }}
            // url={nftState[0].collectionNft.image}
            url={imagePlaceholder}
          />
          <SectionHeadline
            heading={`Collection: ${nftState[0]?.collectionNft.name}`}
            subHead={nftState[0]?.collectionNft.description}
          />
        </div>

        <SearchBox placeholder="Search your favourite NFTs" handleSearch={handleSearchNft} />
      </Container>

      <Container>
        <Row gutter={[30, 30]}>
          {nftState && nftState.length > 0 && nftState[0] !== 'not value' ? (
            nftState?.map((item: any, index: number) => {
              const createdDate = formatDate(item.createdAt);

              return (
                <Col key={index} xl={{ span: 8 }} md={{ span: 12 }} xs={{ span: 24 }}>
                  <Card
                    type="discover"
                    // cardBg="primary-background"
                    imagePlaceholder={item.image}
                    cardInfo={
                      <CardInfo
                        info={{
                          cardName: item.name,
                          avatar: item.user?.avatar,
                          userName: item.user?.name,
                        }}
                      />
                    }
                    artistInfo={[
                      {
                        title: 'Price',
                        total: `${item.price} ETH`,
                      },
                      {
                        title: 'Created Date',
                        total: createdDate,
                      },
                    ]}
                  />
                </Col>
              );
            })
          ) : (
            <h5>No data found, please enter complete data</h5>
          )}
        </Row>
      </Container>
    </>
  );
};

export default CollectionPage;
