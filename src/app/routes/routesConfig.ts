export const Epath = {
  // NotFound Page
  notFoundPage: '/notfound',

  // Login Page
  loginPage: '/login',

  // Create Account
  createAccount: '/create-account',

  // Register Confirm Page
  registerConfirmPage: '/register-confirm',

  // Home Page
  homePage: '/',

  // Artist Page
  artistPage: '/artist-page',

  // NFTs Page
  nftPage: '/nft-page',

  // Collection Page
  collectionPage: '/collection-page',

  // Connect Wallet
  marketplacePage: '/marketplace-page',

  // Connect Wallet
  connectWallet: '/connect-wallet',

  // Rankings Page
  rankingsPage: '/rankings-page',
};
