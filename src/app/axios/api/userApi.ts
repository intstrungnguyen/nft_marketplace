import { axiosClient } from '../axiosClient';

interface IUserDetail {
  name: string;
  email: string;
  avatar: string;
  banner: string;
  bio: string;
}

const patchName = 'users';

const userApi = {
  getUserDetail: async (token: string | null) => {
    const res = axiosClient.get(`${patchName}/detail`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return res;
  },

  getUserById: async (id: string) => {
    const res = axiosClient.get(`${patchName}/${id}`);

    return res;
  },

  getUserRanking: async () => {
    const res = axiosClient.get(`${patchName}/ranking`);

    return res;
  },

  updateCurrentUser: async (data: IUserDetail) => {
    const res = axiosClient.patch(`${patchName}/detail`, {
      name: data.name,
      email: data.email,
      avatar: data.avatar,
      banner: data.banner,
      bio: data.bio,
    });
    return res;
  },
};

export default userApi;
