import { IndexedObject } from 'types/common';
import { axiosClient } from '../axiosClient';
import { createQueryUrl } from 'app/helpers/functions';

const pathName = 'nfts';

const nftApi = {
  getAllNft: async () => {
    const res = axiosClient.get(pathName);
    return res;
  },

  getNFTQuery: async (query: IndexedObject) => {
    const url = createQueryUrl({ pathname: pathName }, query);
    const res = axiosClient.get(url);

    return res;
  },

  getOneNft: async (id: string) => {
    const res = axiosClient.get(`${pathName}/${id}`);
    return res;
  },

  createNft: async (data: any) => {
    const res = axiosClient.post(`${pathName}`, data);
    return res;
  },

  updateNft: async (id: string, data: any) => {
    const res = axiosClient.patch(`${pathName}/${id}`, data);
    return res;
  },

  sellNft: async (id: string) => {
    const res = axiosClient.patch(`${pathName}/sell-or-cancel-sell/${id}`);
    return res;
  },

  buyNft: async (id: string) => {
    const res = axiosClient.patch(`${pathName}/buy-nft/${id}`);
    return res;
  },

  giveNft: async (nftId: string, receiverId: string) => {
    const res = axiosClient.patch(`${pathName}/give/${nftId}`, {
      userId: receiverId,
    });

    return res;
  },
};

export default nftApi;
