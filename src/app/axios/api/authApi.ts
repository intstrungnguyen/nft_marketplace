import { axiosClient } from '../axiosClient';
import { TAccount } from 'types/common';

const pathName = 'auth';

const authApi = {
  createAccount: async (data: TAccount) => {
    const res = await axiosClient.post(`${pathName}/register`, {
      name: data.name,
      email: data.email,
      password: data.password,
    });

    return res;
  },

  login: async (data: any) => {
    const res = await axiosClient.post(`${pathName}/login`, {
      email: data.email,
      password: data.password,
    });

    return res;
  },
};

export default authApi;
