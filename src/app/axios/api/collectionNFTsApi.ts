import { IndexedObject } from 'types/common';
import { axiosClient } from '../axiosClient';
import { createQueryUrl } from 'app/helpers/functions';

const pathname = 'collectionNfts';

const collectionApi = {
  getCollection: async () => {
    const res = await axiosClient.get(pathname);
    return res;
  },

  getCollectionQuery: async (query: IndexedObject) => {
    const url = createQueryUrl({ pathname: pathname }, query);
    const res = await axiosClient.get(url);
    return res;
  },
};

export default collectionApi;
