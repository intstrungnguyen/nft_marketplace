import { axiosClient } from '../axiosClient';

const patchName = 'common';

const commonApi = {
  getOverview: async () => {
    const res = axiosClient.get(`${patchName}/overview`);

    return res;
  },
};

export default commonApi;
