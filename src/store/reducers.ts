/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from '@reduxjs/toolkit';

import { InjectedReducersType } from 'utils/types/injector-typings';

import progressReducer from 'utils/@reduxjs/slice/progressSlice';
import authReducer from 'utils/@reduxjs/slice/authSlice';
import collectionReducer from '../utils/@reduxjs/slice/collectionSlice';
import nftReducer from 'utils/@reduxjs/slice/nftSlice';
import userReducer from 'utils/@reduxjs/slice/userSlice';
import commonReducer from 'utils/@reduxjs/slice/commonSlice';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export const rootReducers = {
  progress: progressReducer,
  auth: authReducer,
  collection: collectionReducer,
  nfts: nftReducer,
  user: userReducer,
  common: commonReducer,
};

export function createReducer(injectedReducers: InjectedReducersType = {}) {
  // Initially we don't have any injectedReducers, so returning identity function to avoid the error
  if (Object.keys(injectedReducers).length === 0) {
    return (state: any) => state;
  } else {
    return combineReducers({
      ...injectedReducers,
    });
  }
}
