import { createSlice } from '../toolkit';
import { PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  loading: true,
  error: false,
  percent: 20,
};

const progressSlice = createSlice({
  name: 'progress',
  initialState: initialState,
  reducers: {
    setProgress(state, action: PayloadAction<number>) {
      state.percent = action.payload;
    },

    setLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },

    setError(state, action: PayloadAction<boolean>) {
      state.error = action.payload;
    },
  },
});

export const { actions, reducer: progressReducer } = progressSlice;
export const { setProgress, setLoading, setError } = actions;
export default progressSlice.reducer;
