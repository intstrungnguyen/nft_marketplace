import { AxiosError } from 'axios';
import { message } from 'antd';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import { TUserDetail } from 'types/common';
import userApi from 'app/axios/api/userApi';
import authApi from 'app/axios/api/authApi';
import {
  saveToken,
  removeToken,
  saveRefreshToken,
  removeRefreshToken,
  saveUser,
  removeUser,
} from 'app/helpers/localStorage';
import { setLoading, setProgress } from './progressSlice';
import { useHistory } from 'react-router-dom';
import { Epath } from 'app/routes/routesConfig';

const initialState: RootState['auth'] = {
  loadingAuth: false,
  error: false,
  authenticated: false,
  currentUser: null,
};

type TResponseLogin = {
  user: any;
  tokens: any;
};

type TActionParams<T> = {
  data: T;
  navigate?: string;
};

type TSignInRequest = {};

type TRegisterRequest = {
  name?: string;
  email: string;
  password: string;
};

type TErrorApiResponse = {
  message: string;
};

export const getCurrentUser = createAsyncThunk(
  'users/detail',
  async (token: string | null, { dispatch }) => {
    await userApi
      .getUserDetail(token)
      .then((res) => {
        const dataUser: TUserDetail = res.data;
        dispatch(setCurrentUser(dataUser));
      })
      .catch(() => dispatch(logOut()));
  },
);

export const registerAction = createAsyncThunk(
  'auth/registerAction',
  async (actionParams: TActionParams<TRegisterRequest>, { dispatch }) => {
    const { navigate, data } = actionParams;
    const history = useHistory();

    dispatch(setLoadingAuth(true));
    dispatch(setProgress(30));

    await authApi
      .createAccount({
        name: data.name,
        email: data.email,
        password: data.password,
      })
      .then((res) => {
        dispatch(setProgress(100));

        setTimeout(() => {
          history.push(navigate || Epath.loginPage);
          dispatch(setLoading(true));
          dispatch(setProgress(0));
        }, 1000);
      })
      .catch((err: AxiosError<TErrorApiResponse>) => {
        dispatch(setLoadingAuth(false));
        dispatch(setError(true));

        dispatch(setLoading(true));
        dispatch(setProgress(0));

        message.error(err.response?.data.message);
      });
  },
);
export const loginAction = createAsyncThunk(
  'auth/loginAction',
  async (actionParams: TActionParams<TSignInRequest>, { dispatch }) => {
    const { navigate, data } = actionParams;

    dispatch(setLoadingAuth(true));
    dispatch(setProgress(30));

    await authApi
      .login(data)
      .then((res) => {
        dispatch(setProgress(100));

        const dataRes = res.data as unknown as TResponseLogin;
        dispatch(logIn(dataRes));
        dispatch(setLoadingAuth(false));

        setTimeout(() => {
          dispatch(setLoading(true));
          dispatch(setProgress(0));
        }, 1000);
      })
      .catch((err: AxiosError<TErrorApiResponse>) => {
        dispatch(setLoadingAuth(false));
        dispatch(setError(true));
        dispatch(setLoading(true));
        dispatch(setProgress(0));

        message.error(err.response?.data.message);
      });
  },
);

const authSlice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    logIn(state, action: PayloadAction<TResponseLogin>) {
      const tokens = action.payload.tokens;

      state.authenticated = true;
      state.currentUser = action.payload.user;

      saveToken(tokens.access.token);
      saveRefreshToken(tokens.refresh.token);
      saveUser(action.payload.user);
    },
    logOut(state) {
      state.authenticated = false;
      state.currentUser = {};

      removeToken();
      removeRefreshToken();
      removeUser();
    },
    setCurrentUser(state, action: PayloadAction<TUserDetail>) {
      state.currentUser = action.payload;
      saveUser(action.payload);
    },

    setLoadingAuth(state, action: PayloadAction<boolean>) {
      state.loadingAuth = action.payload;
    },

    setError(state, action: PayloadAction<boolean>) {
      state.error = action.payload;
    },
  },
});

export const { actions, reducer: authReducer } = authSlice;
export const { logOut, logIn, setCurrentUser, setLoadingAuth, setError } = actions;
export default authSlice.reducer;
