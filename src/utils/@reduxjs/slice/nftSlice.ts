import { AxiosError } from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import { IndexedObject } from 'types/common';
import { notification } from 'antd';
import nftApi from 'app/axios/api/nftApi';
import { getRefreshTokenFromLocalStorage } from 'app/helpers/localStorage';

type TErrorApiResponse = {
  message: string;
};

type TResponseNfts = {};

const refreshToken = getRefreshTokenFromLocalStorage();

// Actions
export const nftAction = createAsyncThunk('nfts', async (_, { dispatch }) => {
  dispatch(setIsLoading(true));

  await nftApi
    .getAllNft()
    .then((res) => {
      dispatch(setIsLoading(false));

      const dataRes = res.data;
      dispatch(allNFT(dataRes));
    })
    .catch((err: AxiosError<TErrorApiResponse>) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));
    });
});

export const nftQueryAction = createAsyncThunk(
  'nfts/query',
  async (query: IndexedObject, { dispatch }) => {
    dispatch(setIsLoading(true));

    await nftApi
      .getNFTQuery(query)
      .then((res) => {
        dispatch(setIsLoading(false));
        const dataRes = res.data;

        if (query.limit && query.page) dispatch(allNFT(dataRes));

        if (query.sortBy) dispatch(queryNFT(dataRes.results));

        if (query.user) dispatch(nftsCurrentUser(dataRes.results));

        if (query.name || (query.name && query.collectionNft))
          dispatch(getNftByName(dataRes.results));

        if (query.collectionNft && query.limit) dispatch(getNftByCollection(dataRes));

        if ((query.user && query.limit) || (query.user && query.limit && query.page))
          dispatch(getNftByUser(dataRes));
      })
      .catch((err: AxiosError<TErrorApiResponse>) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

export const nftSearchAction = createAsyncThunk(
  'nfts/search',
  async (query: IndexedObject, { dispatch }) => {
    dispatch(setIsLoading(true));

    await nftApi
      .getNFTQuery(query)
      .then((res) => {
        dispatch(setIsLoading(false));
        const dataRes = res.data;

        if (query.name || (query.name && query.collectionNft)) dispatch(getNftByName(dataRes));

        if (query.name && query.limit && query.page) {
          if (dataRes.results.length > 0) dispatch(getNftByName(dataRes));
          if (dataRes.results.length === 0) {
            dispatch(getNftByName(['not result']));
          }
        }

        if (query.collectionNft && query.name && query.limit) {
          if (dataRes.results.length > 0) dispatch(getNftByName(dataRes));
          if (dataRes.results.length === 0) {
            dispatch(getNftByName(['not result']));
          }
        }
      })
      .catch((err: AxiosError<TErrorApiResponse>) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

export const createNftAction = createAsyncThunk('create/nfts', async (data: any, { dispatch }) => {
  dispatch(setIsLoading(true));

  await nftApi
    .createNft(data)
    .then((res) => {
      dispatch(setIsLoading(false));

      const dataRes = res.data;
      dispatch(createNft(dataRes));
    })
    .catch((err: AxiosError<TErrorApiResponse>) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));

      notification.error({
        message: 'Error',
        description: err.response?.data.message,
      });
    });
});

export const updateNftAction = createAsyncThunk('create/nfts', async (data: any, { dispatch }) => {
  dispatch(setIsLoading(true));

  await nftApi
    .updateNft(data.id, data.data)
    .then((res) => {
      const dataRes = res.data;

      dispatch(getNftById(dataRes));
      dispatch(setIsLoading(false));
    })
    .catch((err: AxiosError<TErrorApiResponse>) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));

      notification.error({
        message: 'Error',
        description: err.response?.data.message,
      });
    });
});

export const sellNftAction = createAsyncThunk('sell/nfts', async (id: string, { dispatch }) => {
  dispatch(setIsLoading(true));

  await nftApi
    .sellNft(id)
    .then((res) => {
      dispatch(setError(false));

      const dataRes = res.data;
      dispatch(getNftById(dataRes));
      dispatch(setIsLoading(false));
    })
    .catch((err: AxiosError<TErrorApiResponse>) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));

      notification.error({
        message: 'Error',
        description: err.response?.data.message,
      });
    });
});

export const buyNftAction = createAsyncThunk('buy/nfts', async (id: string, { dispatch }) => {
  dispatch(setIsLoading(true));

  await nftApi
    .buyNft(id)
    .then((res) => {
      dispatch(setError(false));

      const dataRes = res.data;
      dispatch(getNftById(dataRes));
      dispatch(setIsLoading(false));
    })
    .catch((err: AxiosError<TErrorApiResponse>) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));

      notification.error({
        message: 'Error',
        description: err.response?.data.message,
      });
    });
});

export const nftHighlightAction = createAsyncThunk(
  'nfts/highlight',
  async (query: IndexedObject, { dispatch }) => {
    dispatch(setIsLoading(true));

    await nftApi
      .getNFTQuery(query)
      .then((res) => {
        dispatch(setIsLoading(false));

        const dataRes = res.data.results;
        dispatch(highlightNFT(dataRes[0]));
      })
      .catch((err: AxiosError<TErrorApiResponse>) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

export const getNftByIdAction = createAsyncThunk('nfts/id', async (id: string, { dispatch }) => {
  dispatch(setIsLoading(true));

  await nftApi
    .getOneNft(id)
    .then((res) => {
      const dataRes = res.data;
      dispatch(getNftById(dataRes));

      dispatch(setIsLoading(false));
    })
    .catch((err: AxiosError<TErrorApiResponse>) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));
    });
});

export const giveNftAction = createAsyncThunk(
  'nfts/id',
  async (data: { nftId: string; receiverId: string }, { dispatch }) => {
    dispatch(setIsLoading(true));

    await nftApi
      .giveNft(data.nftId, data.receiverId)
      .then((res) => {
        const dataRes = res.data;
        dispatch(getNftById(dataRes));

        dispatch(setIsLoading(false));
      })
      .catch((err: AxiosError<TErrorApiResponse>) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

// Slice
const initialState: RootState['nfts'] = {
  isLoading: false,
  error: false,
  dataNFTs: [],
  dataNftCurrentUser: [],
  dataNFTsLimit: [],
  dataNFTsOfCollection: [],
  dataSearchNFTs: ['not value'],
  nftHighlight: [],
  dataNftByUser: [],
  dataNftById: {},
};

const nftSlice = createSlice({
  name: 'nfts',
  initialState: initialState,
  reducers: {
    allNFT(state, action: PayloadAction<TResponseNfts>) {
      state.dataNFTs = action.payload;
    },

    queryNFT(state, action: PayloadAction<TResponseNfts>) {
      state.dataNFTsLimit = action.payload;
    },

    nftsCurrentUser(state, action: PayloadAction<TResponseNfts>) {
      state.dataNftCurrentUser = action.payload;
    },

    getNftById(state, action: PayloadAction<TResponseNfts>) {
      state.dataNftById = action.payload;
    },

    getNftByName(state, action: PayloadAction<TResponseNfts>) {
      state.dataSearchNFTs = action.payload;
    },

    getNftByCollection(state, action: PayloadAction<TResponseNfts>) {
      state.dataNFTsOfCollection = action.payload;
    },

    getNftByUser(state, action: PayloadAction<TResponseNfts>) {
      state.dataNftByUser = action.payload;
    },

    createNft(state, action: PayloadAction<TResponseNfts>) {
      state.dataNftCurrentUser.unshift(action.payload);
    },

    highlightNFT(state, action: PayloadAction<TResponseNfts>) {
      state.nftHighlight = action.payload;
    },

    setIsLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },

    setError(state, action: PayloadAction<boolean>) {
      state.error = action.payload;
    },
  },
});

export const { actions, reducer: authReducer } = nftSlice;
export const {
  allNFT,
  nftsCurrentUser,
  queryNFT,
  getNftByName,
  getNftById,
  getNftByCollection,
  getNftByUser,
  createNft,
  highlightNFT,
  setIsLoading,
  setError,
} = actions;
export default nftSlice.reducer;
