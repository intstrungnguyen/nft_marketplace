import { AxiosError } from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '../toolkit';
import { getTokenFromLocalStorage, saveUser } from 'app/helpers/localStorage';
import userApi from 'app/axios/api/userApi';
import { setCurrentUser } from './authSlice';
import { TUserDetail } from 'types/common';

// Type
type TResponseUser = {};

// Action

export const userRankingAction = createAsyncThunk('user/ranking', async (_, { dispatch }) => {
  dispatch(setIsLoading(true));

  await userApi
    .getUserRanking()
    .then((res) => {
      dispatch(setIsLoading(false));

      const dataRes = res.data.results;
      dispatch(userRanking(dataRes));
    })
    .catch((err: AxiosError) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));
    });
});

export const userProfileAction = createAsyncThunk('user/detail', async (_, { dispatch }) => {
  dispatch(setIsLoading(true));

  await userApi
    .getUserRanking()
    .then((res) => {
      dispatch(setIsLoading(false));

      const dataRes = res.data.results;
      dispatch(userRanking(dataRes));
    })
    .catch((err: AxiosError) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));
    });
});

export const getUserByIdAction = createAsyncThunk(
  '/user/getById',
  async (id: any, { dispatch }) => {
    dispatch(setIsLoading(true));

    await userApi
      .getUserById(id)
      .then((res) => {
        const dataRes: any = res.data;
        dispatch(getUserById(dataRes));
        dispatch(setIsLoading(false));
      })
      .catch((err: AxiosError) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

export const updateUserAction = createAsyncThunk(
  'update/user/detail',
  async (data: any, { dispatch }) => {
    dispatch(setIsLoading(true));

    await userApi
      .updateCurrentUser(data)
      .then((res) => {
        dispatch(setIsLoading(false));

        const dataRes: any = res.data;
        dispatch(setCurrentUser(dataRes));
      })
      .catch((err: AxiosError) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

// Slice
const initialState: RootState['user'] = {
  isLoading: false,
  error: false,
  dataUserRanking: [],
  dataUserById: {},
};

const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    userRanking(state, action: PayloadAction<TResponseUser>) {
      state.dataUserRanking = action.payload;
    },

    getUserById(state, action: PayloadAction<TResponseUser>) {
      state.dataUserById = action.payload;
    },

    setIsLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },

    setError(state, action: PayloadAction<boolean>) {
      state.error = action.payload;
    },
  },
});

export const { actions, reducer: authReducer } = userSlice;
export const { userRanking, getUserById, setIsLoading, setError } = actions;
export default userSlice.reducer;
