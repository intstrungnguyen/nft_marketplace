import { AxiosError } from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '../toolkit';
import { IndexedObject } from 'types/common';
import collectionApi from 'app/axios/api/collectionNFTsApi';
import nftApi from 'app/axios/api/nftApi';

// Type
type TResponseCollection = {};

// Action
export const collectionAction = createAsyncThunk('collectionNfts', async (_, { dispatch }) => {
  dispatch(setIsLoading(true));

  await collectionApi
    .getCollection()
    .then(async (res) => {
      dispatch(setIsLoading(false));

      const dataRes = res.data;

      const combined = await Promise.all(
        dataRes.results?.map(async (collection: any) => {
          const nftsResponse = await nftApi.getNFTQuery({ collectionNft: collection.id });
          return {
            ...collection,
            nfts: nftsResponse.data.results,
          };
        }),
      );

      dispatch(
        allCollection({
          data: combined,
          page: dataRes.page,
          limit: dataRes.limit,
          totalPages: dataRes.totalPages,
          totalResults: dataRes.totalResults,
        }),
      );
    })
    .catch((err: AxiosError) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));
    });
});

export const collectionQueryAction = createAsyncThunk(
  'collectionNfts/query',
  async (query: IndexedObject, { dispatch }) => {
    dispatch(setIsLoading(true));

    await collectionApi
      .getCollectionQuery(query)
      .then(async (res) => {
        dispatch(setIsLoading(false));

        const dataRes = res.data;

        if (query.limit && query.sortBy) dispatch(queryCollection(dataRes.results));

        if (query.name) {
          const combined = await Promise.all(
            dataRes.results?.map(async (collection: any) => {
              const nftsResponse = await nftApi.getNFTQuery({ collectionNft: collection.id });
              return {
                ...collection,
                nfts: nftsResponse.data.results,
              };
            }),
          );
          dispatch(
            getCollectionByName({
              data: combined,
              page: dataRes.page,
              limit: dataRes.limit,
              totalPages: dataRes.totalPages,
              totalResults: dataRes.totalResults,
            }),
          );
        }
      })
      .catch((err: AxiosError) => {
        dispatch(setIsLoading(false));
        dispatch(setError(true));
      });
  },
);

// Slice
const initialState: RootState['collection'] = {
  isLoading: false,
  error: false,
  dataCollection: [],
  dataCollectionQuery: [],
  dataSearchCollection: [],
};

const collectionSlice = createSlice({
  name: 'collection',
  initialState: initialState,
  reducers: {
    allCollection(state, action: PayloadAction<TResponseCollection>) {
      state.dataCollection = action.payload;
    },

    queryCollection(state, action: PayloadAction<TResponseCollection>) {
      state.dataCollectionQuery = action.payload;
    },

    getCollectionByName(state, action: PayloadAction<TResponseCollection>) {
      state.dataSearchCollection = action.payload;
    },

    setIsLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },

    setError(state, action: PayloadAction<boolean>) {
      state.error = action.payload;
    },
  },
});

export const { actions, reducer: authReducer } = collectionSlice;
export const { allCollection, queryCollection, getCollectionByName, setIsLoading, setError } =
  actions;
export default collectionSlice.reducer;
