import { AxiosError } from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from 'types/RootState';
import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '../toolkit';
import commonApi from 'app/axios/api/commonApi';

// Type
type TResponse = {};

// Action
export const commonAction = createAsyncThunk('common/overview', async (_, { dispatch }) => {
  dispatch(setIsLoading(true));

  await commonApi
    .getOverview()
    .then((res) => {
      dispatch(setIsLoading(false));

      const dataRes = res.data;
      dispatch(getOverview(dataRes));
    })
    .catch((err: AxiosError) => {
      dispatch(setIsLoading(false));
      dispatch(setError(true));
    });
});

// Slice
const initialState: RootState['common'] = {
  isLoading: false,
  error: false,
  dataOvervier: {},
};

const commonSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    getOverview(state, action: PayloadAction<TResponse>) {
      state.dataOvervier = action.payload;
    },

    setIsLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload;
    },

    setError(state, action: PayloadAction<boolean>) {
      state.error = action.payload;
    },
  },
});

export const { actions, reducer: authReducer } = commonSlice;
export const { getOverview, setIsLoading, setError } = actions;
export default commonSlice.reducer;
