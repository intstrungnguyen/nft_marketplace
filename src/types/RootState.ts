// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/*
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
export interface ProgressState {
  loading: boolean;
  error: boolean;
  percent: number;
}

export interface CommonState {
  isLoading: boolean;
  error: boolean;
  dataOvervier: any;
}

export interface AuthState {
  authenticated: boolean;
  error: boolean;
  loadingAuth: boolean;
  currentUser: any;
}

export interface CollectionState {
  isLoading: boolean;
  error: boolean;
  dataCollection: any;
  dataCollectionQuery: any;
  dataSearchCollection: any;
}

export interface NFTsState {
  isLoading: boolean;
  error: boolean;
  dataNFTs: any;
  dataNftCurrentUser: any;
  dataNFTsLimit: any;
  dataSearchNFTs: any;
  dataNFTsOfCollection: any;
  nftHighlight: any;
  dataNftByUser: any;
  dataNftById: any;
}

export interface UserState {
  isLoading: boolean;
  error: boolean;
  dataUserRanking: any;
  dataUserById: any;
}

export interface RootState {
  progress: ProgressState;
  auth: AuthState;
  collection: CollectionState;
  nfts: NFTsState;
  user: UserState;
  common: CommonState;
}
