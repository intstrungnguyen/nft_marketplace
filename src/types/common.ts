export type IndexedObject<V = any> = { [k: string]: V };

export type TAction<T> = {
  type: string;
  payload: IndexedObject<T>;
};

export type Locale = { code: string; name: string; bidi: boolean };

export type TOption<T = string | number> = {
  label: string;
  value?: T;
};

export type Responsive = {
  responsive?: {
    xl?: boolean;
    md?: boolean;
    xs?: boolean;
  };
};

export type TAccount = {
  name?: string;
  email: string;
  password: string;
};

export type TUserDetail = {
  id: string;
  email: string;
  name: string;
  role: 'admin' | 'user';
  isEmailVerified: boolean;
  avatar: string;
  banner: string;
  bio: string;
  eth: number;
  createdAt: Date;
  updatedAt: Date;
};

export type TUserRanking = {
  _id?: string;
  id: string;
  avatar: string;
  name: string;
  email: string;
  countNfts: number;
  totalPrice: number;
};
